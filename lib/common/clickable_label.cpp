#include "clickable_label.h"

ClickableLabel::ClickableLabel(QWidget* parent)
    : QLabel(parent)
{

}

bool ClickableLabel::is_checked() const
{
    return m_checked;
}

void ClickableLabel::set_checked(bool check)
{
    if (m_checked != check)
    {
        m_checked = check;
        update_icon();
        emit checked(m_checked);
    }
}

void ClickableLabel::set_unselected_icon(const QString &icon)
{
    m_unselected_icon = QPixmap(icon);
    update_icon();
}

void ClickableLabel::set_selected_icon(const QString &icon)
{
    m_selected_icon = QPixmap(icon);
    update_icon();
}

void ClickableLabel::update_icon()
{
    if (m_checked)
    {
        setPixmap(m_selected_icon);
    }
    else
    {
        setPixmap(m_unselected_icon);
    }
}

void ClickableLabel::mousePressEvent(QMouseEvent *ev)
{
    m_checked = !m_checked;
    update_icon();
    emit checked(m_checked);
    QWidget::mousePressEvent(ev);
}
