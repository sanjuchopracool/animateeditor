#ifndef CLICKABLE_LABEL_H
#define CLICKABLE_LABEL_H

#include <QLabel>

class ClickableLabel : public QLabel
{
    Q_OBJECT
public:
    ClickableLabel(QWidget* parent= nullptr);

    bool is_checked() const;
    void set_checked(bool check);

    void set_unselected_icon(const QString& icon);
    void set_selected_icon(const QString& icon);

signals:
    void checked(bool);

private:
    void update_icon();

protected:
    void mousePressEvent(QMouseEvent *ev) override;

private:
    bool m_checked = false;
    QPixmap m_selected_icon;
    QPixmap m_unselected_icon;
};

#endif // CLICKABLE_LABEL_H
