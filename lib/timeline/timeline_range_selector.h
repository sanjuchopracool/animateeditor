#ifndef TIMELINE_RANGE_SELECTOR_H
#define TIMELINE_RANGE_SELECTOR_H

#include <QWidget>

class TimelineRuler;
class TimeLineVisibleRangeSelector;
class TimeLinePlayRangeSelector;
class TimeLine;

class TimelineRangeSelector : public QWidget
{
    Q_OBJECT
public:
    explicit TimelineRangeSelector(TimeLine * timeline, QWidget *parent = nullptr);

    void update_theme();
signals:

private:
    TimeLine * m_timeline;
    TimelineRuler * m_ruler = nullptr;
    TimeLineVisibleRangeSelector * m_visible_range_selector;
    TimeLinePlayRangeSelector * m_play_range_selector;
};

#endif // TIMELINE_RANGE_SELECTOR_H
