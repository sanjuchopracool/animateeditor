#include "timeline.h"

#include <QDataStream>
#include <QVersionNumber>


namespace
{
QVersionNumber s_version(1, 0, 0);
}

TimeLine::TimeLine(QObject *parent)
    : QObject(parent)
{
    m_timeline = new QTimeLine(m_composition_duration, this);
    m_timeline->setUpdateInterval(m_msec_per_frame);
    m_timeline->setCurveShape(QTimeLine::LinearCurve);
    m_timeline->setLoopCount(0);

    set_composition_duration(10000);
    set_range(0, 10000);
    set_play_range(0, 10000);

    connect(m_timeline, SIGNAL(frameChanged(int)),
            this, SIGNAL(play_frame_changed(int)));
}

TimeLine::~TimeLine()
{

}

qint32 TimeLine::play_duration() const
{
    return m_timeline->duration();
}

void TimeLine::set_play_range(qint32 play_start, qint32 play_end)
{
    if ( (play_end > play_start) and is_valid_range_value(play_start) and is_valid_range_value(play_end))
    {
        if ((play_start != this->play_start()) or (play_end != this->play_end()))
        {
            m_timeline->setDuration(play_end - play_start);
            m_timeline->setFrameRange(play_start, play_end);
            emit play_range_changed();
        }
    }
}

qint32 TimeLine::play_start() const
{
    return m_timeline->startFrame();
}

qint32 TimeLine::play_end() const
{
    return m_timeline->endFrame();
}

qint32 TimeLine::play_frame() const
{
    return m_timeline->currentFrame();
}

void TimeLine::set_range(qint32 range_start, qint32 range_end)
{
    if (is_valid_range_value(range_start) and is_valid_range_value(range_end))
    {
        if (m_range_end != range_end || m_range_start != range_start)
        {
            m_range_start = range_start;
            m_range_end = range_end;

            // update scale also
            m_current_scale = composition_duration() / range_duration();
            emit scale_changed(m_current_scale);
            emit range_changed();
        }
    }
}

qint32 TimeLine::range_duration() const
{
    return (m_range_end - m_range_start);
}

void TimeLine::set_range_start(qint32 range_start)
{
    if (m_range_start != range_start and is_valid_range_value(range_start))
    {
        m_range_start = range_start;
        emit range_changed();
    }
}

qint32 TimeLine::range_start() const
{
    return m_range_start;
}

void TimeLine::set_range_end(qint32 range_end)
{
    if (m_range_end != range_end and is_valid_range_value(range_end))
    {
        m_range_end = range_end;
        emit range_changed();
    }
}

qint32 TimeLine::range_end() const
{
    return m_range_end;
}

void TimeLine::set_composition_duration(qint32 msec)
{
    m_composition_duration = msec;

    if (m_timeline->duration() > m_composition_duration)
        m_timeline->setDuration(m_composition_duration);

    if (m_timeline->endFrame() > m_composition_duration)
        m_timeline->setEndFrame(m_composition_duration);
}

qint32 TimeLine::composition_duration() const
{
    return m_composition_duration;
}

void TimeLine::set_frame_per_second(double fps)
{
    m_msec_per_frame = 1000/fps;
    m_timeline->setUpdateInterval(m_msec_per_frame);
}

qint32 TimeLine::msec_per_frame() const
{
    return m_msec_per_frame;
}

void TimeLine::set_current_frame(qint32 frame)
{
    if (frame >= m_range_start && frame <= m_range_end)
    {
        if (m_current_frame != frame)
        {
            m_current_frame = frame;
            emit selected_frame_changed(m_current_frame);
        }
    }
}

double TimeLine::current_scale() const
{
    return m_current_scale;
}

qint32 TimeLine::current_frame() const
{
    return m_current_frame;
}

qint32 TimeLine::duration() const
{
    return m_timeline->duration();
}

QTimeLine::State TimeLine::state() const
{
    return m_timeline->state();
}

bool TimeLine::is_running() const
{
    return (m_timeline->state() == QTimeLine::Running);
}

void TimeLine::start()
{
    m_timeline->start();
}

void TimeLine::resume()
{
    m_timeline->resume();
}

void TimeLine::stop()
{
    m_timeline->stop();
}

bool TimeLine::save(QDataStream &out) const
{
    out << s_version;

    out << m_composition_duration;
    out << m_range_start;
    out << m_range_end;
    out << m_current_frame;

    out << m_msec_per_frame;
    out << (qint32) m_timeline->startFrame();
    out << (qint32) m_timeline->endFrame();

    return true;
}

bool TimeLine::load(QDataStream &in_stream)
{
    QVersionNumber version;
    in_stream >> version;

    if (version.isNull())
        return false;

    qint32 value;
    in_stream >> value;
    set_composition_duration(value);

    in_stream >> value;
    set_range_start(value);

    in_stream >> value;
    set_range_end(value);

    in_stream >> value;
    set_current_frame(value);

    in_stream >> m_msec_per_frame;
    m_timeline->setUpdateInterval(value);

    in_stream >> value;
    m_timeline->setStartFrame(value);

    in_stream >> value;
    m_timeline->setEndFrame(value);

    m_timeline->setDuration(m_timeline->endFrame() - m_timeline->startFrame());

    double scale;
    in_stream >> scale;

    return true;
}

void TimeLine::scale_range(double scale)
{
    if (scale < 1.0)
        scale = 1.0;


    const qint32 current_range_duration = range_duration();
    double new_range_duration = current_range_duration / scale;
    if (new_range_duration > msec_per_frame())
    {
        qint32 current_frame = this->current_frame();

        if (current_frame < 0)
            current_frame = 0;

        if (current_frame > m_composition_duration)
            current_frame = m_composition_duration;

        double current_start_fraction = static_cast<double>(current_frame - m_range_start)/(m_range_end - m_range_start);
        qint32 new_start = current_frame - (current_start_fraction*new_range_duration);
        if (new_start < 0)
            new_start = 0;

        qint32 new_end = new_start + new_range_duration;
        if (new_end > current_range_duration)
        {
            new_end = current_range_duration;
            new_start = new_end - new_range_duration;
        }

        set_range(new_start, new_start + new_range_duration);
    }
}

bool TimeLine::is_valid_range_value(qint32 value)
{
    return (value >= 0) && (value <= m_composition_duration);
}

bool TimeLine::is_in_range(qint32 value)
{
    return (value >= m_range_start) && (value <= m_range_end);
}
