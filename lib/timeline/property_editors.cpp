#include "property_editors.h"

#include "inode.h"
#include "property.h"

#include <property_editor_item.h>
#include <double_propery_spinbox.h>
#include <double_pair_editor.h>
#include "keyframe_container_widget.h"

#include <QtGlobal>
#include <QString>
#include <limits>

QPair<PropertyEditorItem *, KeyFrameContainerWidget*> PropertyEditors::create_editor(INode *node,
                                                                                     const Property * property,
                                                                                     TimeLine * timeline)
{
    QPair<PropertyEditorItem *, KeyFrameContainerWidget*> pair;
    pair.first = nullptr;
    pair.second = nullptr;

    if (node and property)
    {
        QWidget* editor_widget = nullptr;
        QString property_name;
        PropertyType type = property->type();

        switch (property->type())
        {
        case PropertyType::TransformGroup:
            property_name = QObject::tr("Transform");
            break;
        case PropertyType::RectangleGroup:
            property_name = QObject::tr("Rectangle");
            break;
        case PropertyType::Opacity:
        {
            property_name = QObject::tr("Opacity");
            auto spinbox = DoubleEditorFactory::create_editor(node, type);
            if (spinbox)
            {
                spinbox->setMinimum(0);
                spinbox->setMaximum(1);
                spinbox->setSingleStep(0.01);
                editor_widget = spinbox;
            }
        }
            break;
        case PropertyType::Position:
        {
            property_name = QObject::tr("Position");
            auto pos_editor = DoublePairEditorFactory::create_editor(node, type);
            if (pos_editor)
            {
                pos_editor->set_linkable(false);
                pos_editor->set_single_step(1);
                pos_editor->set_maximum(std::numeric_limits<double>::max());
                pos_editor->set_minimum(-std::numeric_limits<double>::max());
                editor_widget = pos_editor;
            }
        }
            break;
        case PropertyType::AnchorPosition:
        {
            property_name = QObject::tr("Anchor");
            auto pos_editor = DoublePairEditorFactory::create_editor(node, type);
            if (pos_editor)
            {
                pos_editor->set_linkable(false);
                pos_editor->set_single_step(1);
                pos_editor->set_maximum(std::numeric_limits<double>::max());
                pos_editor->set_minimum(-std::numeric_limits<double>::max());
                editor_widget = pos_editor;
            }
        }
            break;
        case PropertyType::Scale:
        {
            property_name = QObject::tr("Scale");
            auto pos_editor = DoublePairEditorFactory::create_editor(node, type);
            if (pos_editor)
            {
                pos_editor->set_linkable(true);
                pos_editor->set_single_step(0.01);
                pos_editor->set_maximum(std::numeric_limits<double>::max());
                pos_editor->set_minimum(0);
                editor_widget = pos_editor;
            }
        }
            break;
        case PropertyType::Rotation:
        {
            property_name = QObject::tr("Rotation");
            auto spinbox = DoubleEditorFactory::create_editor(node, type);
            if (spinbox)
            {
                spinbox->setSingleStep(1);
                spinbox->setMinimum(-std::numeric_limits<double>::max());
                spinbox->setMaximum(std::numeric_limits<double>::max());
                editor_widget = spinbox;
            }
        }
            break;
        case PropertyType::RectSize:
        {
            property_name = QObject::tr("Size");
            auto rect_size_editor = DoublePairEditorFactory::create_editor(node, type);
            if (rect_size_editor)
            {
                rect_size_editor->set_linkable(true);
                rect_size_editor->set_single_step(1.0);
                rect_size_editor->set_maximum(std::numeric_limits<double>::max());
                rect_size_editor->set_minimum(0);
                editor_widget = rect_size_editor;
            }
        }
            break;
        default:
            break;
        }

        if (property_name.size())
        {
            pair.first = new PropertyEditorItem(node, property_name, type, editor_widget);
            pair.second = new KeyFrameContainerWidget(node, type, timeline);
            QObject::connect(pair.first, SIGNAL(expand(bool)),
                             pair.second, SLOT(expand_sub_items(bool)));
            QObject::connect(pair.first, SIGNAL(animation_state_changed(bool)),
                             pair.second, SLOT(update_animation_state(bool)));
            QObject::connect(pair.first, SIGNAL(add_keyframe_clicked()),
                             pair.second, SLOT(slot_add_keyframe_clicked()));
            if (editor_widget)
            {
                QObject::connect(editor_widget, SIGNAL(value_edited()),
                                 pair.second, SLOT(slot_value_edited()));
            }

            if (is_property_group(type))
            {
                for( auto sub_property : property->sub_properties())
                {
                    auto child_pair = create_editor(node, sub_property, timeline);
                    if (child_pair.first)
                    {
                        pair.first->add_sub_item(child_pair.first);
                        pair.second->add_key_frame_widget(child_pair.second);
                    }
                }
            }
        }
    }

    return pair;
}
