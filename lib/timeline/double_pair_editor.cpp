#include "double_pair_editor.h"
#include "ui_double_pair_editor.h"

#include "inode.h"
#include "timeline_theme.h"

DoublePairEditor::DoublePairEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DoublePairEditor)
{
    ui->setupUi(this);
    unsigned row_height = TimelineTheme::row_height();
    setFixedHeight(row_height);
    ui->icon_linked->setFixedSize(row_height, row_height);

    ui->icon_linked->set_selected_icon(":/timeline/link_selected.png");
    ui->icon_linked->set_unselected_icon(":/timeline/link_unselected.png");
    ui->lbl_seperator->setText(", ");
    ui->lbl_seperator->setStyleSheet(QString("color: %1").arg(TimelineTheme::editor_text_color().name()));
}

DoublePairEditor::~DoublePairEditor()
{
    if (m_node)
        m_node->unset_editor(m_type);

    delete ui;
}

void DoublePairEditor::set_info(INode *node, PropertyFunctor *functor, PropertyType type)
{
    m_node = node;
    m_functor = functor;
    m_type = type;

    if (m_node and m_functor)
    {
        set_value(m_functor->m_getter(node).toPointF());
        m_node->set_editor(type, PropertyUpdateTemplate<DoublePairEditor>
                           (this, &DoublePairEditor::set_value));

        ui->editor_x->update_size();
        ui->editor_y->update_size();

        connect(ui->editor_x, SIGNAL(valueChanged(double)),
                this, SLOT(slot_value_changed_x()), Qt::UniqueConnection);
        connect(ui->editor_y, SIGNAL(valueChanged(double)),
                this, SLOT(slot_value_changed_y()), Qt::UniqueConnection);
        connect(ui->icon_linked, SIGNAL(checked(bool)),
                this, SLOT(slot_link_clicked(bool)));

    }
}

void DoublePairEditor::set_linkable(bool link)
{
    ui->icon_linked->setVisible(link);
}

void DoublePairEditor::set_minimum(double min_val)
{
    ui->editor_x->setMinimum(min_val);
    ui->editor_y->setMinimum(min_val);
    set_value(m_functor->m_getter(m_node).toPointF());
}

void DoublePairEditor::set_maximum(double max_val)
{
    ui->editor_x->setMaximum(max_val);
    ui->editor_y->setMaximum(max_val);
    set_value(m_functor->m_getter(m_node).toPointF());
}

void DoublePairEditor::set_single_step(double step)
{
    ui->editor_x->setSingleStep(step);
    ui->editor_y->setSingleStep(step);
}

void DoublePairEditor::slot_value_changed_x()
{
    const qreal x = ui->editor_x->value();
    qreal y = ui->editor_y->value();

    if (ui->icon_linked->is_checked())
    {
        if (y != x)
        {
            ui->editor_y->setValue(x);
            y = x;
        }
    }

    if (m_node)
    {
        m_functor->m_setter(m_node, QPointF(x, y));
        emit value_edited();
    }
}

void DoublePairEditor::slot_value_changed_y()
{
    const qreal y = ui->editor_y->value();
    qreal x = ui->editor_x->value();

    if (ui->icon_linked->is_checked())
    {
        if (x != y)
        {
            ui->editor_x->setValue(y);
            x = y;
        }
    }

    if (m_node)
    {
        m_functor->m_setter(m_node, QPointF(x, y));
        emit value_edited();
    }
}

void DoublePairEditor::slot_link_clicked(bool linked)
{
    if (linked)
    {
        ui->editor_y->setValue(ui->editor_x->value());
    }
}

void DoublePairEditor::set_value(const QVariant &val)
{
    auto point = val.toPointF();
    ui->editor_x->setValue(point.x());
    ui->editor_y->setValue(point.y());
}

DoublePairEditor *DoublePairEditorFactory::create_editor(INode *node, PropertyType type)
{
    if (node)
    {
        auto functors = node->property_functors(type);
        if (functors)
        {
            auto editor =  new DoublePairEditor();
            editor->set_info(node, functors, type);
            return editor;
        }
    }
    return nullptr;
}
