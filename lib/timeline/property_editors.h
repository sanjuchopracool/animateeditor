#ifndef PROPERTY_EDITORS_H
#define PROPERTY_EDITORS_H

#include "property_type.h"
#include <QPair>

class PropertyEditorItem;
class Property;
class INode;
class KeyFrameContainerWidget;
class TimeLine;

class PropertyEditors
{
public:
    static QPair<PropertyEditorItem *, KeyFrameContainerWidget*> create_editor(INode *node,
                                                                               const Property * property,
                                                                               TimeLine *timeline);
};

#endif // PROPERTY_EDITORS_H
