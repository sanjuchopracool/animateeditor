#include "timeline_play_range_selector.h"

#include "timeline_theme.h"
#include "timeline.h"

#include <QPainter>
#include <QStyleOption>
#include <QMouseEvent>
#include <QApplication>

namespace
{
enum EventType
{
    NONE,
    MOVE_START,
    MOVE_END,
    MOVE_RANGE,
};
}

TimeLinePlayRangeSelector::TimeLinePlayRangeSelector(TimeLine *timeline, QWidget *parent)
    : QWidget(parent),
      m_timeline(timeline)
{
    setAttribute(Qt::WA_Hover);
}

void TimeLinePlayRangeSelector::update_theme()
{
    const int row_height = TimelineTheme::visible_range_row_height();
    const int handle_width = TimelineTheme::range_handle_width();
    const QColor handle_color = TimelineTheme::range_handle_color();

    m_left_handle = QPixmap(handle_width, row_height);
    m_right_handle = QPixmap(handle_width, row_height);

    m_left_handle.fill(handle_color);
    m_right_handle.fill(handle_color);
    setFixedHeight(row_height);

    connect(m_timeline, SIGNAL(range_changed()),
            this, SLOT(update()));
    connect(m_timeline, SIGNAL(play_frame_changed(int)),
            this, SLOT(update()));
}

void TimeLinePlayRangeSelector::mousePressEvent(QMouseEvent *event)
{
    m_event_type = NONE;
    m_old_x = event->pos().x();
    if ( m_left_handle_rect.contains(event->pos()))
        m_event_type = MOVE_START;
    else if(m_right_handle_rect.contains(event->pos()))
        m_event_type = MOVE_END;
    else if (m_old_x > m_left_handle_rect.right() && m_old_x < m_right_handle_rect.left())
        m_event_type = MOVE_RANGE;

    if (NONE == m_event_type)
        QWidget::mousePressEvent(event);
    else
        event->accept();
}

void TimeLinePlayRangeSelector::mouseReleaseEvent(QMouseEvent *event)
{
    if (NONE == m_event_type)
        QWidget::mouseReleaseEvent(event);
    else
        event->accept();

    m_event_type = NONE;
}

void TimeLinePlayRangeSelector::mouseMoveEvent(QMouseEvent *event)
{
    if (NONE != m_event_type)
    {
        int range_duration = m_timeline->range_duration();
        int effective_width = width() - 2*TimelineTheme::range_handle_width();
        int delta_x = event->pos().x() - m_old_x;
        m_old_x += delta_x;

        int delta_frames = (delta_x*range_duration)/effective_width;
        int play_start = m_timeline->play_start();
        int play_end = m_timeline->play_end();
        int play_duration = m_timeline->play_duration();
        int comp_duration = m_timeline->composition_duration();
        if (MOVE_START == m_event_type)
        {
            play_start += delta_frames;
        }
        else if (MOVE_END == m_event_type)
        {
            play_end += delta_frames;
        }
        else
        {
            play_start += delta_frames;
            play_end += delta_frames;
        }

        if (play_start < 0)
        {
            play_start = 0;
            if (MOVE_RANGE == m_event_type)
                play_end = play_start + play_duration;
        }

        if (play_end > comp_duration)
        {
            play_end = comp_duration;
            if (MOVE_RANGE == m_event_type)
                play_start = play_end - play_duration;
        }

        m_timeline->set_play_range(play_start, play_end);
        event->accept();
        update();
    }
    else
        QWidget::mouseMoveEvent(event);
}

bool TimeLinePlayRangeSelector::event(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::HoverMove:
    case QEvent::HoverEnter:
    {
        if (should_show_range_cusor(static_cast<QHoverEvent*>(event)->pos()))
        {
            if (not m_overridden_cursor)
            {
                QApplication::setOverrideCursor(Qt::SizeHorCursor);
                m_overridden_cursor = true;
            }
        }
        else
        {
            if (m_overridden_cursor)
            {
                QApplication::restoreOverrideCursor();
                m_overridden_cursor = false;
            }
        }
    }
        return true;
    case QEvent::HoverLeave:
        if (m_overridden_cursor)
        {
            QApplication::restoreOverrideCursor();
            m_overridden_cursor = false;
        }
        return true;
    default:
        break;
    }
    return QWidget::event(event);
}

void TimeLinePlayRangeSelector::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, false);

    const int handle_width = TimelineTheme::range_handle_width();
    int effective_width = width() - 2*handle_width;

    int play_start = m_timeline->play_start();
    int range_start = m_timeline->range_start();
    int play_end = m_timeline->play_end();
    int range_duration = m_timeline->range_duration();
    int play_start_x = ((play_start - range_start)*effective_width)/range_duration + handle_width;
    int play_end_x = ((play_end - range_start)*effective_width)/range_duration + handle_width;
    painter.fillRect(QRect(play_start_x, 0, play_end_x - play_start_x, height()), TimelineTheme::play_range_color());

    m_left_handle_rect = m_left_handle.rect();
    m_left_handle_rect.moveRight(play_start_x);
    painter.drawPixmap(m_left_handle_rect, m_left_handle);
    m_right_handle_rect = m_right_handle.rect();
    m_right_handle_rect.moveLeft(play_end_x);
    painter.drawPixmap(m_right_handle_rect, m_right_handle);
}

bool TimeLinePlayRangeSelector::should_show_range_cusor(QPoint p)
{
    int p_x = p.x();
    bool inside_handles =  (m_left_handle_rect.contains(p) or m_right_handle_rect.contains(p));
    bool between_handles_and_movable = (m_timeline->play_duration() < m_timeline->composition_duration())
            and ((m_left_handle_rect.right() < p_x) and (p_x < m_right_handle_rect.left()));

    return (inside_handles or between_handles_and_movable);
}

void TimeLinePlayRangeSelector::fill_play_range(TimeLine* timeline, QSize size, QPainter *painter, const QColor &color)
{
    const int handle_width = TimelineTheme::range_handle_width();
    int effective_width = size.width() - 2*handle_width;
    int play_start = timeline->play_start();
    int range_start = timeline->range_start();
    int play_end = timeline->play_end();
    int range_duration = timeline->range_duration();
    int play_start_x = ((play_start - range_start)*effective_width)/range_duration + handle_width;
    int play_end_x = ((play_end - range_start)*effective_width)/range_duration + handle_width;
    painter->fillRect(QRect(play_start_x, 0, play_end_x - play_start_x, size.height()), color);
}
