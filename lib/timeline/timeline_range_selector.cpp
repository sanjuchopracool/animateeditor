#include "timeline_range_selector.h"
#include "timeline_ruler.h"
#include "timeline_visible_range_selector.h"
#include "timeline_play_range_selector.h"
#include "timeline_theme.h"

#include <QVBoxLayout>

TimelineRangeSelector::TimelineRangeSelector(TimeLine * timeline, QWidget *parent)
    : QWidget(parent),
      m_timeline(timeline)
{
    auto layout = new QVBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);

    m_visible_range_selector = new TimeLineVisibleRangeSelector(m_timeline);
    layout->addWidget(m_visible_range_selector);

    m_ruler = new TimelineRuler(m_timeline);
    layout->addWidget(m_ruler);

    m_play_range_selector = new TimeLinePlayRangeSelector(m_timeline);
    layout->addWidget(m_play_range_selector);

    setLayout(layout);
}

void TimelineRangeSelector::update_theme()
{
    layout()->setSpacing(TimelineTheme::row_line_width());
    const int handle_width = TimelineTheme::range_handle_width();
    m_ruler->setContentsMargins(handle_width, 0, handle_width, 0);
    m_ruler->setStyleSheet(QString("background-color: %1;").arg(TimelineTheme::ruler_color().name()));
    m_ruler->setFixedHeight(TimelineTheme::ruler_height());
    m_visible_range_selector->update_theme();
    m_play_range_selector->update_theme();
}
