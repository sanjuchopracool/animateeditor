#include "keyframe_container_widget.h"
#include "ui_keyframe_container_widget.h"

#include "timeline_theme.h"
#include "keyframes_widget.h"

#include <QVBoxLayout>

namespace
{
const QString style_sheet("background-color: %1;"
                          "color: %2;");
}

KeyFrameContainerWidget::KeyFrameContainerWidget(INode* node,
                                                 PropertyType type,
                                                 TimeLine *timeline,
                                                 QWidget *parent) :
    QWidget(parent),
    ui(new Ui::KeyFrameContainerWidget),
    m_timeline(timeline)
{
    ui->setupUi(this);
    m_sub_items_layout = new QVBoxLayout;
    ui->child_items_widget->setLayout(m_sub_items_layout);

    ui->child_items_widget->hide();
    ui->widget_main_area->setFixedHeight(TimelineTheme::row_height());
    QVBoxLayout * layout = new QVBoxLayout;
    layout->setMargin(0);
    m_key_frames_widget = new KeyFramesWidget(node, type, m_timeline);
    layout->addWidget(m_key_frames_widget);
    ui->widget_main_area->setLayout(layout);
}

KeyFrameContainerWidget::~KeyFrameContainerWidget()
{
    delete ui;
}

void KeyFrameContainerWidget::add_key_frame_widget(KeyFrameContainerWidget *item)
{
    if (item)
    {
        m_sub_items_layout->addWidget(item);
        m_sub_items.append(item);
    }
}

void KeyFrameContainerWidget::update_theme()
{
    const QString fg_color = TimelineTheme::foreground_color().name();
    setStyleSheet(style_sheet.
                  arg(TimelineTheme::timecontrol_background_color().name()).
                  arg(fg_color));

    ui->child_items_widget->setStyleSheet(style_sheet.
                                          arg(TimelineTheme::timecontrol_line_color().name()).
                                          arg(fg_color));

    unsigned line_width = TimelineTheme::row_line_width();
    ui->verticalLayout->setContentsMargins(0, 0, 0, 0);
    ui->verticalLayout->setSpacing(0);

    m_sub_items_layout->setContentsMargins(0, line_width, 0, 0);
    m_sub_items_layout->setSpacing(line_width);
    for ( auto const item : m_sub_items)
    {
        item->update_theme();
    }
}

void KeyFrameContainerWidget::force_frames_update()
{
    if (m_key_frames_widget)
        m_key_frames_widget->force_frames_update();

    for (auto item : m_sub_items)
        item->force_frames_update();
}

void KeyFrameContainerWidget::expand_sub_items(bool expand)
{
    ui->child_items_widget->setVisible(expand);
}

void KeyFrameContainerWidget::update_animation_state(bool animate)
{
    if (m_key_frames_widget)
        m_key_frames_widget->setEnabled(animate);
}

void KeyFrameContainerWidget::slot_add_keyframe_clicked()
{
    if (m_key_frames_widget)
        m_key_frames_widget->add_key_frame();
}

void KeyFrameContainerWidget::slot_value_edited()
{
    if (m_key_frames_widget)
        m_key_frames_widget->slot_value_edited();
}
