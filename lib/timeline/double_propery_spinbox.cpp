#include "double_propery_spinbox.h"

#include "inode.h"
#include "timeline_theme.h"

#include <QStyle>
#include <QApplication>
#include <QStyleOptionSpinBox>

namespace
{
const QString style_sheet("QDoubleSpinBox{ border: 0px; padding: 0px; padding-right: -%1px; color: %2;}"
                          "QDoubleSpinBox:focus{background:white; color:black;}"
                          "QDoubleSpinBox:disabled{color:%3;}");
}

QDoubleSpinBox *DoubleEditorFactory::create_editor(INode *node, PropertyType type)
{
    if (node)
    {
        auto functors = node->property_functors(type);
        if (functors)
        {
            auto editor =  new DoubleProperySpinBox();
            editor->set_info(node, functors, type);
            return editor;
        }
    }
    return nullptr;
}

DoubleProperySpinBox::DoubleProperySpinBox(QWidget* parent)
    : QDoubleSpinBox(parent)
{
    setFrame(false);
    setButtonSymbols(QSpinBox::NoButtons);
    setFixedHeight(TimelineTheme::row_height());


    connect(this, SIGNAL(valueChanged(QString)),
            this, SLOT(slot_update_size()), Qt::UniqueConnection);

    static int width = -1;
    if (-1 == width)
    {
        auto currentStyle = QApplication::style();
        QStyleOptionSpinBox opt;
        width = currentStyle->subControlRect(QStyle::CC_SpinBox, &opt, QStyle::SC_SpinBoxUp, this).width();
    }

    setStyleSheet(style_sheet.arg(width).arg(TimelineTheme::editor_text_color().name()).
                  arg(TimelineTheme::disabled_text_color().name()));
}

DoubleProperySpinBox::~DoubleProperySpinBox()
{
    if (m_node)
        m_node->unset_editor(m_type);
}

void DoubleProperySpinBox::set_info(INode *node, PropertyFunctor *functor, PropertyType type)
{
    m_node = node;
    m_functor = functor;
    m_type = type;

    if (m_node and m_functor)
    {
        setValue(m_functor->m_getter(node).toDouble());
        m_node->set_editor(type, PropertyUpdateTemplate<DoubleProperySpinBox>
                           (this, &DoubleProperySpinBox::set_value));
        update_size();
        connect(this, SIGNAL(valueChanged(double)),
                this, SLOT(slot_update_size(double)), Qt::UniqueConnection);
    }
}

void DoubleProperySpinBox::update_size()
{
    slot_update_size();
}

void DoubleProperySpinBox::slot_update_size(double val)
{
    if (m_node)
    {
        m_functor->m_setter(m_node, val);
        emit value_edited();
    }
}

void DoubleProperySpinBox::slot_update_size()
{
    QString new_text(textFromValue(value()) + "A");
            int new_length = new_text.length();
    if (m_old_length != new_length)
    {
        m_old_length = new_length;
        QFontMetrics fm(font());
        setFixedWidth(fm.width(new_text));
    }
}

void DoubleProperySpinBox::set_value(const QVariant &val)
{
    setValue(val.toDouble());
}
