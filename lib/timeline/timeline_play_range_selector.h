#ifndef TIMELINE_PLAY_RANGE_SELECTOR_H
#define TIMELINE_PLAY_RANGE_SELECTOR_H

#include <QWidget>
class QPixmap;
class TimeLine;

class TimeLinePlayRangeSelector : public QWidget
{
    Q_OBJECT
public:
    explicit TimeLinePlayRangeSelector(TimeLine* timeline, QWidget *parent = nullptr);

    void update_theme();

    static void fill_play_range(TimeLine* timeline, QSize size, QPainter* painter, const QColor& color);
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    bool event(QEvent *event) override;

public slots:

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    bool should_show_range_cusor(QPoint p);
private:
    TimeLine * m_timeline = nullptr;
    QPixmap m_left_handle;
    QPixmap m_right_handle;

    QRect m_left_handle_rect;
    QRect m_right_handle_rect;
    int m_event_type;
    int m_old_x = 0;
    bool m_overridden_cursor = false;
};

#endif // TIMELINE_PLAY_RANGE_SELECTOR_H
