#ifndef TIMELINE_RULER_H
#define TIMELINE_RULER_H

#include <QWidget>

class TimeLine;
class TimelineRuler : public QWidget
{
    Q_OBJECT
public:
    explicit TimelineRuler(TimeLine * timeline, QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent *) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    int frame_for_click(int x);

private:
    TimeLine * m_timeline = nullptr;
    QPixmap m_timeline_mark;
    QRect m_marker_rect;
    bool m_reset = false;
    bool m_dragging = false;

    QPixmap m_pix;
    QRect m_old_rect;
};

#endif // TIMELINE_RULER_H
