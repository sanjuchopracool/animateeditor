#ifndef TIMELINE_WIDGET_H
#define TIMELINE_WIDGET_H

#include <QFrame>
#include <memory>

class Composition;
class INode;
class TimeLine;

struct TimelineWidgetData;
class TimelineWidget : public QFrame
{
    Q_OBJECT
public:
    explicit TimelineWidget(Composition *node,QWidget *parent = nullptr);
    ~TimelineWidget();

    QString name() const;
    Composition *composition() const;

    void add_node(INode* node);
    void remove_node(INode* node);

    int items_count() const;

    void update_size();

signals:
    void range_changed();

private slots:
    void save_splitter_state();

    // time line
    void slot_frame_changed(int frame);
    void slot_selected_frame_changed(int frame);

private:
    void update_theme();
    void update_parent_options_for_nodes();

private:
    int min_property_editor_width() const;

private:
     std::unique_ptr<TimelineWidgetData> d;
};

#endif // TIMELINE_WIDGET_H
