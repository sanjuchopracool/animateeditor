#include "timeline_widget.h"
#include "timeline_node_item.h"

#include "timeline_theme.h"
#include "composition.h"
#include "timeline_conrol_widget.h"
#include "timeline_item_control_widget.h"
#include "user_settings.h"
#include "timeline.h"
#include "timeline_editor_widget.h"

#include <QPixmap>
#include <QVBoxLayout>
#include <QList>
#include <QPainter>
#include <QDir>
#include <QTemporaryFile>
#include <QSplitter>
#include <QTimer>
#include <QScrollArea>
#include <QScrollBar>

namespace
{
const QString style_sheet("background-color: %1;"
                          "color: %2;");
}

struct TimelineWidgetData
{
    ~TimelineWidgetData()
    {

    }

    QList<QPair<INode*, TimelineNodeItem*>> node_items;
    TimelineEditorWidget *property_widget = nullptr;
    TimelineConrolWidget *timeline_control_widget = nullptr;
    QSplitter *splitter = nullptr;
    TimeLine * timeline = nullptr;
    Composition * composition = nullptr;
};

TimelineWidget::TimelineWidget(Composition *node, QWidget *parent)
    : QFrame(parent),
      d (new TimelineWidgetData)
{
    d->composition = node;
    d->timeline = node->timeline();
    QHBoxLayout *main_h_layout = new QHBoxLayout;
    main_h_layout->setMargin(0);
    main_h_layout->setSpacing(0);


    // property item
    d->property_widget = new TimelineEditorWidget;

    connect(d->timeline, SIGNAL(play_frame_changed(int)),
            this, SLOT(slot_frame_changed(int)));
    connect(d->timeline, SIGNAL(selected_frame_changed(int)),
            this, SLOT(slot_selected_frame_changed(int)));

    // timeline time control
    d->timeline_control_widget = new TimelineConrolWidget(d->timeline);

    // splitter
    d->splitter = new QSplitter;
    d->splitter->setChildrenCollapsible(false);
    d->splitter->addWidget(d->property_widget);
    d->splitter->addWidget(d->timeline_control_widget);
    d->splitter->setStretchFactor(0,0);
    d->splitter->setStretchFactor(1,1);
    main_h_layout->addWidget(d->splitter);
    d->splitter->restoreState(UserSettings::timeline_splitter_state());

    setLayout(main_h_layout);
    update_theme();

    connect(d->splitter, SIGNAL(splitterMoved(int,int)),
            this, SLOT(save_splitter_state()));

    connect(d->property_widget, &TimelineEditorWidget::scroll_value_changed,
            this, [=](int value)
    {
        d->timeline_control_widget->blockSignals(true);
        d->timeline_control_widget->set_scroll_value(value);
        d->timeline_control_widget->blockSignals(false);
    });

    connect(d->timeline_control_widget, &TimelineConrolWidget::scroll_value_changed,
            this, [=](int value)
    {
        d->property_widget->blockSignals(true);
        d->property_widget->set_scroll_value(value);
        d->property_widget->blockSignals(false);
    });
}

TimelineWidget::~TimelineWidget()
{
}

QString TimelineWidget::name() const
{
    return d->composition->name();
}

Composition *TimelineWidget::composition() const
{
    return d->composition;
}

void TimelineWidget::add_node(INode *node)
{
    if (node)
    {
        for ( const auto& pair : d->node_items)
        {
            if (pair.first == node)
                return;
        }

        auto item = new TimelineNodeItem(node, d->timeline);
        item->update_theme();

        d->property_widget->add_widget(item);
        d->node_items.append(qMakePair(node, item));
        auto time_control_item = item->timeline_control_item();
        time_control_item->update_theme();
        d->timeline_control_widget->add_item(time_control_item);
        update_parent_options_for_nodes();
    }

    update_size();
}

void TimelineWidget::remove_node(INode *node)
{
    if (node)
    {
        for (int i = 0; i < d->node_items.size(); ++i)
        {
            if (d->node_items[i].first == node)
            {
                d->property_widget->remove_widget(d->node_items[i].second);

                delete d->node_items[i].second;
                d->node_items.removeAt(i);
                update_parent_options_for_nodes();
            }
        }
    }
}

int TimelineWidget::items_count() const
{
    int count = 0;
    for(const auto item : d->node_items)
    {
        count += item.second->items_count();
    }
    return count;
}

void TimelineWidget::update_size()
{
//    d->property_widget->setFixedHeight((items_count() + 2)*(TimelineTheme::row_height() + TimelineTheme::row_line_width()));
}

void TimelineWidget::save_splitter_state()
{
    UserSettings::save_timeline_splitter_state(d->splitter->saveState());
}

void TimelineWidget::slot_frame_changed(int frame)
{
    for (const auto& item : d->node_items)
        item.first->update_animation(frame);
}

void TimelineWidget::slot_selected_frame_changed(int frame)
{
    d->timeline_control_widget->update_timeline_mark();
    if (d->timeline->state() != QTimeLine::Running)
    {
        slot_frame_changed(frame);
    }
}

void TimelineWidget::update_theme()
{
    const QString fg_color = TimelineTheme::foreground_color().name();
    setStyleSheet(style_sheet.
                  arg(TimelineTheme::property_background_color().name()).
                  arg(fg_color));

    d->splitter->setHandleWidth(1);
    d->splitter->setStyleSheet(QString("QSplitter::handle {"
                                       "background-color: %1;"
                                       "}").arg(TimelineTheme::timecontrol_line_color().name()));

    d->property_widget->setStyleSheet(style_sheet.
                                      arg(TimelineTheme::property_line_color().name()).
                                      arg(fg_color));

    d->timeline_control_widget->update_theme();
    d->property_widget->update_theme();
    d->timeline_control_widget->update();
}

void TimelineWidget::update_parent_options_for_nodes()
{
    QList<INode*> nodes;
    nodes.append(d->composition);
    for (const auto& item : d->node_items)
        nodes.append(item.first);

    for (const auto& item : d->node_items)
        item.second->update_parent_options(nodes);
}

int TimelineWidget::min_property_editor_width() const
{
    int max_width = 0;
    for (const auto& item : d->node_items)
        max_width = std::max(item.second->min_width(), max_width);
    return max_width;
}
