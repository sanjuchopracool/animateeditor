#ifndef TIMELINE_ITEM_CONTROL_WIDGET_H
#define TIMELINE_ITEM_CONTROL_WIDGET_H

#include <QWidget>

class INode;
class QVBoxLayout;
class QHBoxLayout;
class KeyFrameContainerWidget;
class TimeLine;
class TimelineDragItem;

class TimelineItemControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TimelineItemControlWidget(INode * node, TimeLine * timeline, QWidget *parent = nullptr);
    ~TimelineItemControlWidget();

    void add_key_frame_widget(KeyFrameContainerWidget* item);

    void update_theme();
    void set_color(const QColor& color);

public slots:
    void expand_sub_items(bool expand);
    void force_frames_update();

private:
    INode * m_node = nullptr;
    TimeLine * m_timeline = nullptr;
    QVBoxLayout *m_sub_items_layout = nullptr;
    QList<KeyFrameContainerWidget*> m_sub_items;
    QWidget * m_child_items_widget;
    TimelineDragItem * m_drag_item;
};

#endif // TIMELINE_ITEM_CONTROL_WIDGET_H
