#ifndef TIMELINE_DRAG_ITEM_H
#define TIMELINE_DRAG_ITEM_H

#include <QFrame>

namespace Ui {
class TimelineDragItem;
}

class INode;
class TimeLine;

class TimelineDragItem : public QFrame
{
    Q_OBJECT

public:
    explicit TimelineDragItem(INode *node, TimeLine * timeline, QWidget *parent = nullptr);
    ~TimelineDragItem();

    void set_color(const QColor& color);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

    void paintEvent(QPaintEvent *) override;

signals:
    void node_animation_time_changed();

private:
    bool is_inside_item_range(int x) const;

private:
    Ui::TimelineDragItem *ui;
    class INode *m_node;
    TimeLine* m_timeline;
    bool m_dragging = false;
    bool m_timeline_playing = false;
    int m_old_x  = 0;
    QPixmap m_drag_icon;
    QRect m_icon_rect;
    QColor m_color;
};

#endif // TIMELINE_DRAG_ITEM_H
