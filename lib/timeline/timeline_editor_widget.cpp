#include "timeline_editor_widget.h"

#include <timeline_theme.h>
#include <QVBoxLayout>

#include <QScrollArea>
#include <QScrollBar>
#include <QSlider>

TimelineEditorWidget::TimelineEditorWidget(QWidget *parent)
    : QFrame(parent)
{
    auto layout = new QVBoxLayout;
    layout->setContentsMargins(0, TimelineTheme::ruler_height() + 3*TimelineTheme::row_line_width() + 2*TimelineTheme::visible_range_row_height(), 0, 0);

    m_main_widget = new QWidget;
    QVBoxLayout * internal_layout = new QVBoxLayout;
    internal_layout->setMargin(0);
    internal_layout->setSpacing(0);

    m_property_item_layout = new QVBoxLayout;
    internal_layout->addLayout(m_property_item_layout);
    internal_layout->addStretch();
    m_main_widget->setLayout(internal_layout);

    m_scroll_area = new QScrollArea;
    m_scroll_area->setContentsMargins(0, 0, 0, 0);
    m_scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_scroll_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_scroll_area->horizontalScrollBar()->setEnabled(false);
    m_scroll_area->setWidgetResizable( true );
    m_scroll_area->setWidget(m_main_widget);
    layout->addWidget(m_scroll_area);
    setLayout(layout);
    update_theme();

    connect(m_scroll_area->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SIGNAL(scroll_value_changed(int)));
}

TimelineEditorWidget::~TimelineEditorWidget()
{

}

void TimelineEditorWidget::update_theme()
{
    unsigned line_width = TimelineTheme::row_line_width();
    layout()->setSpacing(line_width);
    m_property_item_layout->setContentsMargins(0, 0, 0, 0);
    m_property_item_layout->setSpacing(line_width);
}

void TimelineEditorWidget::add_widget(QWidget *item)
{
    m_property_item_layout->addWidget(item);
}

void TimelineEditorWidget::remove_widget(QWidget *item)
{
    m_property_item_layout->removeWidget(item);
}

void TimelineEditorWidget::set_scroll_value(int value)
{
    m_scroll_area->verticalScrollBar()->setValue(value);
}
