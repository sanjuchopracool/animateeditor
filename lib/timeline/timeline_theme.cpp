#include "timeline_theme.h"

namespace
{
const int m_row_height = 24;
const int m_row_line_width = 1;
const QColor m_property_background_color{"#262626"};
const QColor m_foreground_color{Qt::white};
const QColor m_editor_text_color{"#1F79E7"};
const QColor m_property_line_color{Qt::black};
const QColor m_disabled_text_color{"#5F5F5F"};
const QColor m_timecontrol_background_color{"#454545"};
const QColor m_timecontrol_line_color{Qt::black};
const QColor m_timecontrol_line_marker_color{"#00CCFF"};
const int m_timeline_marker_line_width = 1;

const QColor m_ruler_color = Qt::black;
const QColor m_ruler_text_color = Qt::white;
const int m_ruler_height = 20;

const int m_range_handle_width = 15;
const int m_visible_range_row_height = 15;
const QColor m_range_handle_color{"#2C3A47"};
const QColor m_range_color{"#535c68"};
const QColor m_play_range_color{"#848f9d"};
const QColor m_play_range_line_color{"#da1f26"};
}

int TimelineTheme::row_height()
{
    return m_row_height;
}

int TimelineTheme::row_line_width()
{
    return m_row_line_width;
}

const QColor &TimelineTheme::property_background_color()
{
    return m_property_background_color;
}

const QColor &TimelineTheme::property_line_color()
{
    return m_property_line_color;
}

const QColor &TimelineTheme::foreground_color()
{
    return m_foreground_color;
}

const QColor &TimelineTheme::editor_text_color()
{
    return m_editor_text_color;
}

const QColor &TimelineTheme::disabled_text_color()
{
    return m_disabled_text_color;
}

const QColor &TimelineTheme::timecontrol_background_color()
{
    return m_timecontrol_background_color;
}

const QColor &TimelineTheme::timecontrol_line_color()
{
    return m_timecontrol_line_color;
}

const QColor &TimelineTheme::timecontrol_line_marker_color()
{
    return m_timecontrol_line_marker_color;
}

int TimelineTheme::timeline_marker_line_width()
{
    return m_timeline_marker_line_width;
}

const QColor &TimelineTheme::ruler_color()
{
    return m_ruler_color;
}

const QColor &TimelineTheme::ruler_text_color()
{
    return m_ruler_text_color;
}

int TimelineTheme::ruler_height()
{
    return m_ruler_height;
}

int TimelineTheme::min_scale_unit_width()
{
    return 2*m_ruler_height;
}

int TimelineTheme::range_handle_width()
{
    return m_range_handle_width;
}

int TimelineTheme::visible_range_row_height()
{
    return m_visible_range_row_height;
}

QColor TimelineTheme::play_range_color()
{
    return m_play_range_color;
}

QColor TimelineTheme::range_handle_color()
{
    return m_range_handle_color;
}

QColor TimelineTheme::range_color()
{
    return m_range_color;
}

QColor TimelineTheme::play_range_line_color()
{
    return m_play_range_line_color;
}
