#include "keyframes_widget.h"

#include "timeline_theme.h"
#include "inode.h"
#include "timeline.h"
#include "timeline_play_range_selector.h"

#include <QVBoxLayout>
#include <QLabel>

#include <QPainter>
#include <QStyleOption>
#include <QMouseEvent>

namespace
{
const int keyframe_affinity = 7;
}

KeyFramesWidget::KeyFramesWidget(INode *node,
                                 PropertyType type, TimeLine *timeline,
                                 QWidget *parent)
    : QWidget(parent),
      m_node(node),
      m_type(type),
      m_timeline(timeline),
      m_keyframe_pix("://timeline/keyframe-gray.png"),
      m_keyframe_selected_pix("://timeline/keyframe-blue.png")
{
    setEnabled(node->is_animating(type));
    m_is_not_group = not is_property_group(type);
    setFocusPolicy(Qt::ClickFocus);
}

void KeyFramesWidget::force_frames_update()
{
    m_dirty = true;
    update();
}

void KeyFramesWidget::add_key_frame()
{
    if (m_node->is_animating(m_type))
    {
        m_node->add_keyframe(m_type, m_timeline->current_frame(), m_node->property_value(m_type));
        m_dirty = true;
        update();
    }
}

void KeyFramesWidget::slot_value_edited()
{
    qWarning() <<Q_FUNC_INFO;
    if (not m_timeline->is_running())
    {
        if (m_node->is_animating(m_type))
        {
            m_node->update_keyframe_value(m_type, m_timeline->current_frame(), m_node->property_value(m_type));
        }
    }
}

void KeyFramesWidget::mousePressEvent(QMouseEvent *event)
{
    // TODO handle multiple selection
    m_selected_frames.clear();

    m_click_x = event->pos().x();
    const int handle_width = TimelineTheme::range_handle_width();
    int click_x = m_click_x - handle_width;
    int effective_width = width() - 2*handle_width;

    int range_start = m_timeline->range_start();
    long range_duration = m_timeline->range_duration();

    KeyFrameTime time_for_x = (range_duration*click_x)/effective_width + range_start;
    KeyFrameTime affinity_time = (keyframe_affinity*range_duration)/effective_width;

    KeyFrameTime t_range_start = time_for_x - affinity_time;
    KeyFrameTime t_range_end = time_for_x + affinity_time;
    for ( auto keyframe : m_keyframes)
    {
        KeyFrameTime t = keyframe->time();
        if ( t >= t_range_start && t <= t_range_end)
        {
            //            if (not m_selected_frames.contains(keyframe))
            //            {
            m_selected_frames.append(keyframe);
            m_timeline->set_current_frame(t);
            break;
            //            }
        }
    }
    event->accept();
    update();
}

void KeyFramesWidget::mouseReleaseEvent(QMouseEvent *event)
{
    QWidget::mouseReleaseEvent(event);
}

void KeyFramesWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (m_selected_frames.size())
    {
        int new_click_x = event->pos().x();
        int movement = new_click_x - m_click_x;
        if (movement)
        {

            const int handle_width = TimelineTheme::range_handle_width();
            int effective_width = width() - 2*handle_width;
            long range_duration = m_timeline->range_duration();

            KeyFrameTime time_for_movement = (range_duration*movement)/effective_width;
            if (time_for_movement)
            {
                for(int i = 0; i < m_selected_frames.count(); ++i )
                {
                    auto frame = m_selected_frames[i];
                    auto new_frame = m_node->update_keyframe(m_type, frame, frame->time() + time_for_movement);
                    m_timeline->set_current_frame(new_frame->time());
                    if (new_frame != frame)
                    {
                        m_selected_frames.replace(i, new_frame);
                        m_dirty = true;
                    }
                }
                update();
                m_click_x = new_click_x;
            }
        }

        event->accept();
    }
    else
        QWidget::mouseMoveEvent(event);
}

void KeyFramesWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);

    auto range_color = TimelineTheme::play_range_color();
    range_color.setAlpha(75);
    TimeLinePlayRangeSelector::fill_play_range(m_timeline, size(), &painter, range_color);

    if (m_is_not_group && isEnabled())
    {
        if (m_dirty)
        {
            m_keyframes = m_node->keyframes(m_type);
            m_dirty = false;
        }

        if (m_keyframes.size())
        {
            const int handle_width = TimelineTheme::range_handle_width();
            int start = m_timeline->range_start();
            int end = m_timeline->range_end();
            double scale = (width() - 2*handle_width)/static_cast<double>(end - start);
            QRect rect = m_keyframe_pix.rect();
            int h = height();

            double extra_frames = (handle_width + m_keyframe_pix.width())/ scale;
            int start_draw_range = start - extra_frames;
            int end_draw_range = end + extra_frames;
            for(const auto& frame : m_keyframes)
            {
                // keyframes will be sorted by time
                KeyFrameTime t  = frame->time();
                if (t >= start_draw_range)
                {
                    if (t > end_draw_range)
                        break;

                    rect.moveCenter(QPoint(handle_width + scale*(t - start), h/2));
                    if (m_selected_frames.contains(frame))
                        painter.drawPixmap(rect, m_keyframe_selected_pix);
                    else
                        painter.drawPixmap(rect, m_keyframe_pix);
                }
            }
        }
    }
}

void KeyFramesWidget::keyPressEvent(QKeyEvent *event)
{
    bool is_delete_key = (event->key() == Qt::Key_Delete);

#ifdef Q_OS_MACOS
    is_delete_key |= (event->key() == Qt::Key_Backspace);
#endif

    if (m_selected_frames.size() && is_delete_key && (event->modifiers() == Qt::NoModifier))
    {
        for(auto item : m_selected_frames)
        {
            m_node->remove_keyframe(m_type, item);
        }
        m_selected_frames.clear();
        m_dirty = true;
        update();
        event->accept();
    }
    else
        QWidget::keyPressEvent(event);
}

void KeyFramesWidget::focusOutEvent(QFocusEvent *event)
{
    m_selected_frames.clear();
    QWidget::focusOutEvent(event);
}
