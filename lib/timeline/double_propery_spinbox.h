#ifndef DOUBLE_PROPERTY_SPINBOX_H
#define DOUBLE_PROPERTY_SPINBOX_H

#include "property_type.h"
#include "property_functor.h"
#include <QDoubleSpinBox>

class INode;
class DoubleEditorFactory
{
public:
    static QDoubleSpinBox * create_editor(INode *node, PropertyType type);
};

class DoubleProperySpinBox : public QDoubleSpinBox
{
    Q_OBJECT
public:
    DoubleProperySpinBox(QWidget* parent = nullptr);
    ~DoubleProperySpinBox();

    void set_info(INode *node,
                  PropertyFunctor * functor,
                  PropertyType type);

    void update_size();

signals:
    void value_edited();

private slots:
    void slot_update_size(double);
    void slot_update_size();

private:
    void set_value(const QVariant& val);

private:
    INode * m_node = nullptr;
    PropertyFunctor * m_functor = nullptr;
    PropertyType m_type;
    int m_old_length = 0;
};

#endif // DOUBLE_PROPERTY_SPINBOX_H
