#include "timeline_node_item.h"
#include "ui_timeline_node_item.h"

#include <inode.h>
#include <property.h>
#include <property_editors.h>
#include <property_editor_item.h>
#include "timeline_item_control_widget.h"

#include "timeline_theme.h"

namespace
{
const QString style_sheet("background-color: %1;"
                          "color: %2;");

const QString line_edit_style_sheet("QLineEdit{ border: 0px; padding: 0px;}"
                                    "QLineEdit:focus{background:white; color:black;}");
}

TimelineNodeItem::TimelineNodeItem(INode *node, TimeLine *timeline, QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::TimelineNodeItem),
    m_node(node),
    m_timeline(timeline)
{
    m_ui->setupUi(this);

    unsigned row_height = TimelineTheme::row_height();
    m_ui->lbl_expand->setFixedSize(row_height, row_height);
    m_ui->lbl_lock->setFixedSize(row_height, row_height);
    m_ui->lbl_visible->setFixedSize(row_height, row_height);
    m_ui->line_edit_name->setText(m_node->name());
    m_ui->frame->setFixedHeight(row_height);

    m_property_item_layout = new QVBoxLayout;
    m_ui->property_widget->setLayout(m_property_item_layout);

    m_control_item = new TimelineItemControlWidget(node, m_timeline);

    m_ui->lbl_expand->set_selected_icon(":/timeline/arrow-down.png");
    m_ui->lbl_expand->set_unselected_icon(":/timeline/arrow-right.png");

    m_ui->lbl_lock->set_selected_icon(":/timeline/lock_selected.png");
    m_ui->lbl_lock->set_unselected_icon(":/timeline/lock_unselected.png");

    m_ui->lbl_visible->set_selected_icon(":/timeline/eye_selected.png");
    m_ui->lbl_visible->set_unselected_icon(":/timeline/eye_unselected.png");

    connect(m_ui->lbl_expand, SIGNAL(checked(bool)),
            this, SLOT(slot_expand(bool)));

    connect(m_ui->lbl_lock, SIGNAL(checked(bool)),
            this, SLOT(set_lock_icon(bool)));
    connect(m_ui->lbl_visible, SIGNAL(checked(bool)),
            this, SLOT(set_visible(bool)));
    connect(m_ui->line_edit_name, SIGNAL(textChanged(QString)),
            this, SLOT(set_name(QString)));
    connect(m_ui->comboBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(parent_index_changed(int)));

    if (m_node)
    {
        slot_expand(m_ui->lbl_expand->is_checked());
        set_lock_icon(m_ui->lbl_lock->is_checked());
        m_ui->lbl_visible->set_checked(m_node->is_visible());

        for( const auto property : m_node->properties())
        {
            auto editors = PropertyEditors::create_editor(node, property, m_timeline);
            if (editors.first)
            {
                m_property_item_layout->addWidget(editors.first);
                editors.first->update_theme();

                m_property_items.append(editors.first);
                m_control_item->add_key_frame_widget(editors.second);
            }
        }
    }

    update_theme();
}

TimelineNodeItem::~TimelineNodeItem()
{
    delete m_ui;
}

int TimelineNodeItem::items_count() const
{
    int count = 1;
    for (const auto item : m_property_items)
        count += item->items_count();

    return count;
}

int TimelineNodeItem::min_width() const
{
    int min_width = minimumSizeHint().width();
    for (const auto item : m_property_items)
        min_width = std::max(item->min_width(), min_width);
    return min_width;
}

TimelineItemControlWidget *TimelineNodeItem::timeline_control_item() const
{
    return m_control_item;
}

void TimelineNodeItem::update_theme()
{
    setStyleSheet(style_sheet.
                  arg(TimelineTheme::property_background_color().name()).
                  arg(TimelineTheme::foreground_color().name()));

    m_ui->property_widget->setStyleSheet(style_sheet.
                                         arg(TimelineTheme::property_line_color().name()).
                                         arg(TimelineTheme::foreground_color().name()));

    unsigned line_width = TimelineTheme::row_line_width();
    m_property_item_layout->setContentsMargins(0, 0, 0, 0);
    m_property_item_layout->setSpacing(line_width);
    m_ui->verticalLayout->setSpacing(line_width);
    m_ui->line_edit_name->setStyleSheet(line_edit_style_sheet);
    m_ui->frame->setFixedHeight(TimelineTheme::row_height());
}

void TimelineNodeItem::update_parent_options(QList<INode *> nodes)
{
    QSignalBlocker blocker(m_ui->comboBox);
    m_ui->comboBox->clear();

    int index = 0;
    int item_index = 0;
    for (auto node : nodes)
    {
        if (node == m_node)
            continue;

        QString text;
        if (node->parent())
            text = node->name();
        else
            text = tr("None");

        m_ui->comboBox->addItem(text, QVariant::fromValue(node));

        if (node == m_node->parent())
            item_index = index;

        index++;
    }

    m_ui->comboBox->setCurrentIndex(item_index);
}

void TimelineNodeItem::slot_expand(bool checked)
{
    m_ui->property_widget->setVisible(checked);
    m_control_item->expand_sub_items(checked);
}

void TimelineNodeItem::set_lock_icon(bool checked)
{
    for ( auto item : m_property_items)
    {
        item->set_lock(checked);
    }
}

void TimelineNodeItem::set_visible(bool checked)
{
    if (m_node)
        m_node->set_visible(checked);
}

void TimelineNodeItem::set_name(const QString &name)
{
    m_node->set_name(name);
}

void TimelineNodeItem::parent_index_changed(int)
{

    INode* new_parent = qvariant_cast<INode*>(m_ui->comboBox->currentData());
    if (new_parent->parent() == m_node)
    {
        qWarning() << "Can not set child as parent!!";
        QSignalBlocker blocker(m_ui->comboBox);
        int index = m_ui->comboBox->findData(QVariant::fromValue(m_node->parent()));
        m_ui->comboBox->setCurrentIndex(index);
    }
    else
    {
        new_parent->add_child(m_node);
    }

}

void TimelineNodeItem::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    for ( auto item : m_property_items)
    {
        item->set_left_indent(m_ui->line_edit_name->geometry().left());
    }
}
