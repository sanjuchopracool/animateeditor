INCLUDEPATH +=$$PWD

FORMS += \
    $$PWD/property_editor_item.ui \
    $$PWD/timeline_node_item.ui \
    $$PWD/double_pair_editor.ui \
    $$PWD/timeline_drag_item.ui \
    $$PWD/keyframe_container_widget.ui

HEADERS += \
    $$PWD/property_editor_item.h \
    $$PWD/property_editors.h \
    $$PWD/timeline_node_item.h \
    $$PWD/double_pair_editor.h \
    $$PWD/double_propery_spinbox.h \
    $$PWD/timeline_item_control_widget.h \
    $$PWD/timeline_drag_item.h \
    $$PWD/keyframe_container_widget.h \
    $$PWD/timeline_conrol_widget.h \
    $$PWD/timeline_theme.h \
    $$PWD/timeline_ruler.h \
    $$PWD/timeline_widget.h \
    $$PWD/timeline.h \
    $$PWD/keyframes_widget.h \
    $$PWD/timeline_visible_range_selector.h \
    $$PWD/timeline_play_range_selector.h \
    $$PWD/timeline_range_selector.h \
    $$PWD/timeline_editor_widget.h

SOURCES += \
    $$PWD/property_editors.cpp \
    $$PWD/timeline_node_item.cpp \
    $$PWD/property_editor_item.cpp \
    $$PWD/double_pair_editor.cpp \
    $$PWD/double_propery_spinbox.cpp \
    $$PWD/timeline_item_control_widget.cpp \
    $$PWD/timeline_drag_item.cpp \
    $$PWD/keyframe_container_widget.cpp \
    $$PWD/timeline_conrol_widget.cpp \
    $$PWD/timeline_theme.cpp \
    $$PWD/timeline_ruler.cpp \
    $$PWD/timeline_widget.cpp \
    $$PWD/timeline.cpp \
    $$PWD/keyframes_widget.cpp \
    $$PWD/timeline_visible_range_selector.cpp \
    $$PWD/timeline_play_range_selector.cpp \
    $$PWD/timeline_range_selector.cpp \
    $$PWD/timeline_editor_widget.cpp
