#include "property_editor_item.h"
#include "ui_property_editor_item.h"

#include "inode.h"
#include "timeline_theme.h"

#include <QKeyEvent>
#include <QDataStream>
#include <QClipboard>
#include <QMimeData>

namespace
{
const QString style_sheet("background-color: %1;"
                          "color: %2;");

const QString mime_id("EDITOR::TIMELINE::PropertyEditorItem");
}

PropertyEditorItem::PropertyEditorItem(INode* node,
                                       const QString &name,
                                       PropertyType type,
                                       QWidget *editor,
                                       QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::PropertyEditorItem),
    m_node(node),
    m_type(type)
{
    m_ui->setupUi(this);
    setFocusPolicy(Qt::ClickFocus);

    unsigned row_height = TimelineTheme::row_height();
    m_ui->lbl_expand->setFixedSize(row_height, row_height);
    m_ui->lbl_animate->setFixedSize(row_height, row_height);
    m_ui->frame->setFixedHeight(row_height);

    m_property_item_layout = new QVBoxLayout;
    m_ui->property_widget->setLayout(m_property_item_layout);

    m_ui->lbl_expand->set_selected_icon(":/timeline/arrow-down.png");
    m_ui->lbl_expand->set_unselected_icon(":/timeline/arrow-right.png");

    m_ui->lbl_animate->set_selected_icon(":/timeline/watch_selected.png");
    m_ui->lbl_animate->set_unselected_icon(":/timeline/watch_unselected.png");

    connect(m_ui->lbl_expand, SIGNAL(checked(bool)),
            this, SLOT(slot_expand(bool)));

    connect(m_ui->lbl_animate, SIGNAL(checked(bool)),
            this, SLOT(slot_animate(bool)));

    bool is_group = is_property_group(type);

    slot_expand(m_ui->lbl_expand->is_checked());
    m_ui->lbl_expand->setVisible(is_group);
    m_ui->lbl_animate->setHidden(is_group);

    if (node)
        m_ui->lbl_animate->set_checked(node->is_animating(type));

    m_ui->lbl_name->setText(name);
    set_editor_widget(editor);
    update_theme();

    if (not is_group)
    {
        m_add_keyframe_label = new ClickableLabel;
        m_add_keyframe_label->setFixedSize(row_height, row_height);
        m_add_keyframe_label->set_selected_icon(":/timeline/keyframe-gray.png");
        m_add_keyframe_label->set_unselected_icon(":/timeline/keyframe-gray.png");
        m_ui->horizontalLayout->insertWidget(0, m_add_keyframe_label);
        QSizePolicy sp_retain = m_add_keyframe_label->sizePolicy();
        sp_retain.setRetainSizeWhenHidden(true);
        m_add_keyframe_label->setSizePolicy(sp_retain);

        m_add_keyframe_label->setVisible(m_node->is_animating(m_type));
        connect(m_add_keyframe_label, SIGNAL(checked(bool)), this, SIGNAL(add_keyframe_clicked()));
    }
}

PropertyEditorItem::~PropertyEditorItem()
{
    delete m_ui;
}

void PropertyEditorItem::set_editor_widget(QWidget *editor)
{
    if(editor)
    {
        m_editor = editor;
        int index = m_ui->horizontalLayout->indexOf(m_ui->horizontalSpacer);
        m_ui->horizontalLayout->insertWidget(index, m_editor);
    }
}

void PropertyEditorItem::copy_animations(QDataStream &out) const
{
    out << m_type;
    auto keyFrames = m_node->keyframes(m_type);
    out << m_node->is_animating(m_type);
    out << keyFrames.size();
    for (auto frame : keyFrames)
    {
        out << frame->time();
        out << frame->value();
    }

    for ( auto const item : m_sub_items)
    {
        item->copy_animations(out);
    }
}

void PropertyEditorItem::paste_animations(QDataStream &in_stream)
{
    PropertyType type;
    in_stream >> type;

    if (type == m_type)
    {
        bool is_animating = false;
        in_stream >> is_animating;

        int no_of_keyframes = 0;
        in_stream >> no_of_keyframes;

        m_node->clear_keyframes(m_type);

        for (int i= 0; i< no_of_keyframes; ++i)
        {
            KeyFrameTime t;
            QVariant value;

            in_stream >> t;
            in_stream >> value;
            m_node->add_keyframe(m_type, t, value);
        }

        slot_animate(is_animating);

        for ( auto const item : m_sub_items)
        {
            item->paste_animations(in_stream);
        }
    }
}

void PropertyEditorItem::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    for ( auto item : m_sub_items)
    {
        item->set_left_indent(m_ui->lbl_name->geometry().left() - TimelineTheme::row_height());
    }
}

void PropertyEditorItem::keyPressEvent(QKeyEvent *event)
{
    auto key_sequence = QKeySequence(event->modifiers()|event->key());
    if (QKeySequence(QKeySequence::Copy) == key_sequence)
        copy_animations();
    else if (QKeySequence(QKeySequence::Paste) == key_sequence)
        paste_animations();
    else
        QWidget::keyPressEvent(event);
}

void PropertyEditorItem::update_theme()
{
    const QString fg_color = TimelineTheme::foreground_color().name();
    setStyleSheet(style_sheet.
                  arg(TimelineTheme::property_background_color().name()).
                  arg(fg_color));

    m_ui->property_widget->setStyleSheet(style_sheet.
                                         arg(TimelineTheme::property_line_color().name()).
                                         arg(fg_color));

    unsigned line_width = TimelineTheme::row_line_width();

    m_ui->verticalLayout->setContentsMargins(0, 0, 0, 0);
    m_ui->verticalLayout->setSpacing(0);

    m_property_item_layout->setContentsMargins(0, line_width, 0, 0);
    m_property_item_layout->setSpacing(line_width);
    m_ui->frame->setFixedHeight(TimelineTheme::row_height());

    for ( auto const item : m_sub_items)
    {
        item->update_theme();
    }
}

void PropertyEditorItem::set_left_indent(int indent)
{
    m_ui->left_spacer->setFixedWidth(indent);
}

int PropertyEditorItem::left_indent() const
{
    return m_ui->left_spacer->width();
}

void PropertyEditorItem::add_sub_item(PropertyEditorItem *child)
{
    if (child)
    {
        m_property_item_layout->addWidget(child);
        m_sub_items.append(child);
    }
}

void PropertyEditorItem::set_lock(bool lock)
{
    if (m_editor)
        m_editor->setDisabled(lock);

    for ( auto const item : m_sub_items)
    {
        item->set_lock(lock);
    }
}

int PropertyEditorItem::items_count() const
{
    int count = 1;
    for (const auto item : m_sub_items)
        count += item->items_count();

    return count;
}

int PropertyEditorItem::min_width() const
{
    int min_width = minimumSizeHint().width();
    for (const auto item : m_sub_items)
        min_width = std::max(item->min_width(), min_width);
    return min_width;
}

void PropertyEditorItem::slot_expand(bool checked)
{
    m_ui->property_widget->setVisible(checked);
    emit expand(checked);
}

void PropertyEditorItem::slot_animate(bool animate)
{
    if (m_node)
    {
        m_node->animate_property(m_type, animate);

        if (m_add_keyframe_label)
            m_add_keyframe_label->setVisible(animate);
    }

    emit animation_state_changed(animate);
}

void PropertyEditorItem::copy_animations()
{
    QByteArray data;
    QDataStream data_stream(&data, QIODevice::WriteOnly);
    copy_animations(data_stream);
    QMimeData* mime_data = new QMimeData;
    mime_data->setData(mime_id, data);
    qApp->clipboard()->setMimeData(mime_data);
}

void PropertyEditorItem::paste_animations()
{
    const QMimeData* mime_data = qApp->clipboard()->mimeData();
    if (mime_data)
    {
        QByteArray data = mime_data->data(mime_id);
        if (data.size())
        {
            QDataStream data_stream(&data, QIODevice::ReadOnly);
            paste_animations(data_stream);
        }
    }
}
