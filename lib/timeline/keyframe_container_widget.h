#ifndef KEYFRAME_CONTAINER_WIDGET_H
#define KEYFRAME_CONTAINER_WIDGET_H

#include <QWidget>

#include "property_type.h"

class INode;
class QVBoxLayout;
class KeyFrame;
class KeyFramesWidget;
class TimeLine;


namespace Ui {
class KeyFrameContainerWidget;
}

class KeyFrameContainerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit KeyFrameContainerWidget(INode* node,
                                     PropertyType type,
                                     TimeLine * timeline,
                                     QWidget *parent = nullptr);
    ~KeyFrameContainerWidget();

    void add_key_frame_widget(KeyFrameContainerWidget* item);

    void update_theme();
    void force_frames_update();

public slots:
    void expand_sub_items(bool expand);
    void update_animation_state(bool animate);
    void slot_add_keyframe_clicked();
    void slot_value_edited();

private:
    void update_frame();

private:
    Ui::KeyFrameContainerWidget *ui;
    TimeLine * m_timeline;
    QVBoxLayout *m_sub_items_layout = nullptr;
    QList<KeyFrameContainerWidget*> m_sub_items;
    KeyFramesWidget * m_key_frames_widget = nullptr;
};

#endif // KEYFRAME_CONTAINER_WIDGET_H
