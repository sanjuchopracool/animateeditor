#ifndef TIMELINE_H
#define TIMELINE_H

#include <QTimeLine>

class TimeLine : public QObject
{
    Q_OBJECT
public:
    TimeLine(QObject* parent = nullptr);
    ~TimeLine();

    qint32 play_duration() const;
    void set_play_range(qint32 play_start, qint32 play_end);

    qint32 play_start() const;
    qint32 play_end() const;
    qint32 play_frame() const;

    void set_range(qint32 range_start, qint32 range_end);
    qint32 range_duration() const;

    void set_range_start(qint32 range_start);
    qint32 range_start() const;

    void set_range_end(qint32 range_end);
    qint32 range_end() const;

    void set_composition_duration(qint32 msec);
    qint32 composition_duration() const;

    void set_frame_per_second(double fps);
    qint32 msec_per_frame() const;

    void set_current_frame(qint32 frame);
    qint32 current_frame() const;

    double current_scale() const;

    qint32 duration() const;
    QTimeLine::State state() const;
    bool is_running() const;

    void start();
    void resume();
    void stop();

    bool save(QDataStream &out) const;
    bool load(QDataStream &in_stream) ;

public slots:
    void scale_range(double scale);

signals:
    void selected_frame_changed(int);
    void play_frame_changed(int);
    void range_changed();
    void play_range_changed();
    void scale_changed(double);

private:
    bool is_valid_range_value(qint32 value);
    bool is_in_range(qint32 value);
private:
    QTimeLine* m_timeline = nullptr;
    qint32 m_composition_duration = 1000;
    qint32 m_range_start = 0;
    qint32 m_range_end = 1000;
    qint32 m_current_frame = 0;

    qint32 m_msec_per_frame = 40; //25fps
    double m_current_scale = 1;
};

#endif // TIMELINE_H
