#include "timeline_visible_range_selector.h"

#include "timeline_theme.h"
#include "timeline.h"

#include <QPainter>
#include <QStyleOption>
#include <QMouseEvent>
#include <QApplication>

namespace
{
enum EventType
{
    NONE,
    MOVE_START,
    MOVE_END,
    MOVE_RANGE,
};
}

TimeLineVisibleRangeSelector::TimeLineVisibleRangeSelector(TimeLine *timeline, QWidget *parent)
    : QWidget(parent),
      m_timeline(timeline)
{
    setAttribute(Qt::WA_Hover);
}

void TimeLineVisibleRangeSelector::update_theme()
{
    const int row_height = TimelineTheme::visible_range_row_height();
    const int handle_width = TimelineTheme::range_handle_width();
    const QColor handle_color = TimelineTheme::range_handle_color();

    m_left_handle = QPixmap(handle_width, row_height);
    m_right_handle = QPixmap(handle_width, row_height);

    m_left_handle.fill(handle_color);
    m_right_handle.fill(handle_color);
    setFixedHeight(row_height);

    connect(m_timeline, SIGNAL(selected_frame_changed(int)),
            this, SLOT(update()));
    connect(m_timeline, SIGNAL(range_changed()),
            this, SLOT(update()));
    connect(m_timeline, SIGNAL(play_range_changed()),
            this, SLOT(update()));
}

void TimeLineVisibleRangeSelector::mousePressEvent(QMouseEvent *event)
{
    m_event_type = NONE;
    m_old_x = event->pos().x();
    if ( m_left_handle_rect.contains(event->pos()))
        m_event_type = MOVE_START;
    else if(m_right_handle_rect.contains(event->pos()))
        m_event_type = MOVE_END;
    else if (m_old_x > m_left_handle_rect.right() && m_old_x < m_right_handle_rect.left())
        m_event_type = MOVE_RANGE;

    if (NONE == m_event_type)
        QWidget::mousePressEvent(event);
    else
        event->accept();
}

void TimeLineVisibleRangeSelector::mouseReleaseEvent(QMouseEvent *event)
{
    if (NONE == m_event_type)
        QWidget::mouseReleaseEvent(event);
    else
        event->accept();

    m_event_type = NONE;
}

void TimeLineVisibleRangeSelector::mouseMoveEvent(QMouseEvent *event)
{
    if (NONE != m_event_type)
    {
        int comp_duration = m_timeline->composition_duration();
        int effective_width = width() - 2*TimelineTheme::range_handle_width();
        int delta_x = event->pos().x() - m_old_x;
        m_old_x += delta_x;

        int delta_frames = (delta_x*comp_duration)/effective_width;
        int range_start = m_timeline->range_start();
        int range_end = m_timeline->range_end();
        int duration = m_timeline->range_duration();
        if (MOVE_START == m_event_type)
        {
            range_start += delta_frames;
        }
        else if (MOVE_END == m_event_type)
        {
            range_end += delta_frames;
        }
        else
        {
            range_start += delta_frames;
            range_end += delta_frames;
        }

        if (range_start < 0)
        {
            range_start = 0;
            if (MOVE_RANGE == m_event_type)
                range_end = range_start + duration;
        }

        if (range_end > comp_duration)
        {
            range_end = comp_duration;
            if (MOVE_RANGE == m_event_type)
                range_start = range_end - duration;
        }

        m_timeline->set_range(range_start, range_end);
        event->accept();
    }
    else
        QWidget::mouseMoveEvent(event);
}

bool TimeLineVisibleRangeSelector::event(QEvent *event)
{
    switch (event->type())
    {
    case QEvent::HoverMove:
    case QEvent::HoverEnter:
    {
        if (is_inside_handles(static_cast<QHoverEvent*>(event)->pos()))
        {
            if (not m_overridden_cursor)
            {
                QApplication::setOverrideCursor(Qt::SizeHorCursor);
                m_overridden_cursor = true;
            }
        }
        else
        {
            if (m_overridden_cursor)
            {
                QApplication::restoreOverrideCursor();
                m_overridden_cursor = false;
            }
        }
    }
        return true;
    case QEvent::HoverLeave:
        if (m_overridden_cursor)
        {
            QApplication::restoreOverrideCursor();
            m_overridden_cursor = false;
        }
        return true;
    default:
        break;
    }
    return QWidget::event(event);
}

void TimeLineVisibleRangeSelector::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, false);

    const int handle_width = TimelineTheme::range_handle_width();
    int effective_width = width() - 2*handle_width;
    int comp_duration = m_timeline->composition_duration();

    int play_start = m_timeline->play_start();
    int play_duration = m_timeline->play_duration();
    int play_draw_width = (play_duration*effective_width)/comp_duration;
    int play_start_x = (play_start*effective_width)/comp_duration + handle_width;
    painter.fillRect(QRect(play_start_x, 0, play_draw_width, height()), TimelineTheme::play_range_color());

    int range_start = m_timeline->range_start();
    int range_duration = m_timeline->range_duration();
    int range_draw_width = (range_duration*effective_width)/comp_duration;
    int range_start_x = (range_start*effective_width)/comp_duration + handle_width;

    m_left_handle_rect = m_left_handle.rect();
    m_left_handle_rect.moveRight(range_start_x);
    painter.drawPixmap(m_left_handle_rect, m_left_handle);
    m_right_handle_rect = m_right_handle.rect();
    m_right_handle_rect.moveLeft(range_start_x + range_draw_width);
    painter.drawPixmap(m_right_handle_rect, m_right_handle);
    painter.fillRect(QRect(range_start_x, 0, range_draw_width, height()), TimelineTheme::range_color());

    auto pen = painter.pen();
    pen.setWidth(1);
    pen.setColor(TimelineTheme::timecontrol_line_marker_color());
    painter.setPen(pen);
    // we need to draw selected frame line
    int line_x = (m_timeline->current_frame()*effective_width)/comp_duration  + handle_width;
    painter.drawLine(line_x, 0, line_x, height());
}

bool TimeLineVisibleRangeSelector::is_inside_handles(QPoint p)
{
    return (m_left_handle_rect.contains(p) or m_right_handle_rect.contains(p));
}
