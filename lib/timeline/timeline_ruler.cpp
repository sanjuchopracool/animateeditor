#include "timeline_ruler.h"

#include "timeline_theme.h"
#include "timeline.h"

#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>

#include <cmath>

TimelineRuler::TimelineRuler(TimeLine *timeline, QWidget *parent)
    : QWidget(parent),
      m_timeline(timeline),
      m_timeline_mark("://timeline/mark.png"),
      m_marker_rect(QPoint(0, 0), m_timeline_mark.size())
{
    setFixedHeight(TimelineTheme::ruler_height());
    connect(m_timeline, SIGNAL(selected_frame_changed(int)), this, SLOT(update()));
    connect(m_timeline, &TimeLine::range_changed,
            this, [=]()
    {
        this->m_reset = true;
        this->update();
    });
}

#include <QDebug>
void TimelineRuler::paintEvent(QPaintEvent*)
{
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);

    // find current range
    const int start = m_timeline->range_start();
    const int end = m_timeline->range_end();
    const int handle_width = TimelineTheme::range_handle_width();
    const int content_width = contentsRect().width();

    QRect new_rect(rect());
    if ((m_old_rect != new_rect) || m_reset)
    {
        qDebug() << "redrawing ruler";
        m_old_rect = rect();
        m_reset = false;


        m_pix = QPixmap(size());
        m_pix.fill(TimelineTheme::ruler_color());
        QPainter pix_painter(&m_pix);

        pix_painter.setPen(TimelineTheme::ruler_text_color());

        const int ruler_height = TimelineTheme::ruler_height();
        int max_div_count = content_width / TimelineTheme::min_scale_unit_width();

        int range = end - start;

        double scale = static_cast<double>(content_width)/range;

        int time_div = m_timeline->msec_per_frame();
        // check for each frame
        int unit_per_div = 1;
        bool draw_frame_based = false;
        while(unit_per_div <= 5)
        {
            int div_count = range / (time_div*unit_per_div);
            if (div_count <= max_div_count)
            {
                time_div = time_div*unit_per_div;
                draw_frame_based = true;
                break;
            }

            unit_per_div++;
        }

        // check for seconds
        if (not draw_frame_based)
        {
            time_div = 1000;
            unit_per_div = 1;
            while(1)
            {
                int div_count = range / (time_div*unit_per_div);
                if (div_count <= max_div_count)
                {
                    time_div = time_div*unit_per_div;
                    break;
                }

                unit_per_div++;
            }
        }

        QString text("%1s%2");
        // draw seconds
        int top = ruler_height - 5;
        int scale_x_start = (start/time_div)*time_div;;
        for (;scale_x_start <= end; scale_x_start += time_div)
        {
            int left_x = handle_width + (scale_x_start -start)*scale;
            pix_painter.drawLine(left_x, top, left_x, ruler_height);

            // draw text
            QString no_of_frames_text;
            if (draw_frame_based)
                no_of_frames_text = QString::number(((scale_x_start%1000)/time_div)*unit_per_div);

            QString t = text.arg(scale_x_start/1000).arg(no_of_frames_text);
            QFontMetrics fm(font());
            auto t_rect = fm.boundingRect(t);
            t_rect.moveCenter(QPoint(left_x, top - t_rect.height()/2));
            pix_painter.drawText(t_rect, t);
        }
    }

    painter.drawPixmap(rect(), m_pix);

    int currentFrame = m_timeline->current_frame();
    if ( currentFrame >= start && currentFrame <= end)
    {
        int mark_x = handle_width + (currentFrame - start)*content_width/(end - start);
        m_marker_rect.moveBottom(height());
        m_marker_rect.moveCenter(QPoint(mark_x, m_marker_rect.center().y()));
        painter.drawPixmap(m_marker_rect, m_timeline_mark);
    }
}

void TimelineRuler::mousePressEvent(QMouseEvent *event)
{
    if (m_marker_rect.contains(event->pos()))
    {
        m_dragging = true;
    }
    else
    {
        m_timeline->set_current_frame(frame_for_click(event->pos().x()));
    }

    event->accept();
}

void TimelineRuler::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_dragging)
    {
        m_dragging = false;
        m_timeline->set_current_frame(frame_for_click(event->pos().x()));
        event->accept();
    }
    else
        QWidget::mouseReleaseEvent(event);
}

void TimelineRuler::mouseMoveEvent(QMouseEvent *event)
{
    if (m_dragging)
    {
        m_timeline->set_current_frame(frame_for_click(event->pos().x()));
        event->accept();
    }
    else
        QWidget::mouseMoveEvent(event);
}

int TimelineRuler::frame_for_click(int x)
{
    const int handle_width = TimelineTheme::range_handle_width();
    int frame = (m_timeline->range_start() + (m_timeline->range_duration()*(x - handle_width))/contentsRect().width());
    if (frame < m_timeline->range_start())
        frame = m_timeline->range_start();

    if (frame >= m_timeline->range_end())
        frame = m_timeline->range_end();

    return std::round(static_cast<double>(frame)/m_timeline->msec_per_frame())*m_timeline->msec_per_frame();
}
