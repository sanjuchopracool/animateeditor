#ifndef TIMELINE_NODE_ITEM_H
#define TIMELINE_NODE_ITEM_H

#include <QWidget>

class INode;
class PropertyEditorItem;
class QVBoxLayout;
class TimelineItemControlWidget;
class TimeLine;

namespace Ui {
class TimelineNodeItem;
}

class TimelineNodeItem : public QWidget
{
    Q_OBJECT

public:
    explicit TimelineNodeItem(INode* node, TimeLine* timeline, QWidget *parent = nullptr);
    ~TimelineNodeItem();

    int items_count() const;
    int min_width() const;

    TimelineItemControlWidget *timeline_control_item() const;

    void update_theme();
    void update_parent_options(QList<INode*> nodes);

private slots:
    void slot_expand(bool checked);
    void set_lock_icon(bool checked);
    void set_visible(bool checked);
    void set_name(const QString& name);
    void parent_index_changed(int);

protected:
    void showEvent(QShowEvent *event) override;

private:
    Ui::TimelineNodeItem *m_ui;
    INode *m_node = nullptr;
    TimeLine * m_timeline = nullptr;
    QVBoxLayout *m_property_item_layout = nullptr;
    QList<PropertyEditorItem*> m_property_items;
    TimelineItemControlWidget   *m_control_item = nullptr;
};

#endif // TIMELINE_NODE_ITEM_H
