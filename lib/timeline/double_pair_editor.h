#ifndef DOUBLE_PAIR_EDITOR_H
#define DOUBLE_PAIR_EDITOR_H

#include "property_type.h"
#include "property_functor.h"

#include <QWidget>

class INode;

namespace Ui {
class DoublePairEditor;
}

class DoublePairEditor : public QWidget
{
    Q_OBJECT

public:
    explicit DoublePairEditor(QWidget *parent = nullptr);
    ~DoublePairEditor();

    void set_info(INode *node,
                  PropertyFunctor * functor,
                  PropertyType type);

    void set_linkable(bool link = true);

    void set_minimum(double min_val);
    void set_maximum(double max_val);
    void set_single_step(double step);

private slots:
    void slot_value_changed_x();
    void slot_value_changed_y();
    void slot_link_clicked(bool linked);

signals:
    void value_edited();

private:
    void set_value(const QVariant& val);
private:
    Ui::DoublePairEditor *ui;
    INode * m_node = nullptr;
    PropertyFunctor * m_functor = nullptr;
    PropertyType m_type;
};

class DoublePairEditorFactory
{
public:
    static DoublePairEditor *create_editor(INode *node, PropertyType type);
};
#endif // DOUBLE_PAIR_EDITOR_H
