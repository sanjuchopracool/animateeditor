#include "timeline_conrol_widget.h"

#include "timeline_theme.h"
#include "timeline_item_control_widget.h"
#include "timeline.h"
#include "timeline_range_selector.h"

#include <QVBoxLayout>
#include <QMouseEvent>
#include <QPixmap>
#include <QPainter>
#include <QSlider>
#include <QScrollArea>
#include <QScrollBar>

#include <QDebug>

namespace
{
const QString style_sheet("background-color: %1;"
                          "color: %2;");
}

TimelineConrolWidget::TimelineConrolWidget(TimeLine *timeline, QWidget *parent)
    : QFrame(parent),
      m_timeline(timeline)
{
    auto layout = new QVBoxLayout;
    layout->setMargin(0);

    m_range_widget = new TimelineRangeSelector(m_timeline);
    layout->addWidget(m_range_widget);

    m_main_widget = new QWidget;
    QVBoxLayout * internal_layout = new QVBoxLayout;
    internal_layout->setMargin(0);
    internal_layout->setSpacing(0);

    m_timeline_item_layout = new QVBoxLayout;
    internal_layout->addLayout(m_timeline_item_layout);
    internal_layout->addStretch();
    m_main_widget->setLayout(internal_layout);

    m_scroll_area = new QScrollArea;
    m_scroll_area->setContentsMargins(0, 0, 0, 0);
    m_scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_scroll_area->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_scroll_area->horizontalScrollBar()->setEnabled(false);
    m_scroll_area->setWidgetResizable( true );
    m_scroll_area->setWidget(m_main_widget);
    layout->addWidget(m_scroll_area);
    setLayout(layout);
    m_timeline_marker_line = new QWidget(this);
    m_timeline_marker_line->setGeometry(0, 0, 0, 0);
    m_timeline_marker_line->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);

    m_timeline_play_marker_line = new QWidget(this);
    m_timeline_play_marker_line->setGeometry(0, 0, 0, 0);
    m_timeline_play_marker_line->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);

    connect(m_timeline, &TimeLine::range_changed, [=]()
    {
        update_timeline_mark();
    });

    connect(m_timeline, &TimeLine::play_frame_changed, [=]()
    {
        update_timeline_play_mark();
    });

    connect(m_scroll_area->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SIGNAL(scroll_value_changed(int)));
}

TimelineConrolWidget::~TimelineConrolWidget()
{

}

void TimelineConrolWidget::add_item(TimelineItemControlWidget *item)
{
    m_timeline_item_layout->addWidget(item);
    item->stackUnder(m_timeline_marker_line);
}

void TimelineConrolWidget::update_theme()
{
    const unsigned line_width = TimelineTheme::row_line_width();
    layout()->setSpacing(line_width);
    m_timeline_item_layout->setContentsMargins(0, 0, 0, 0);
    m_timeline_item_layout->setSpacing(line_width);
    setStyleSheet(style_sheet.
                  arg(TimelineTheme::timecontrol_line_color().name()).
                  arg(TimelineTheme::foreground_color().name()));

    m_timeline_marker_line->setStyleSheet(QString("background-color: %1;").
                                          arg(TimelineTheme::timecontrol_line_marker_color().name()));

    m_timeline_play_marker_line->setStyleSheet(QString("background-color: %1;").
                                          arg(TimelineTheme::play_range_line_color().name()));
    m_range_widget->update_theme();
}

void TimelineConrolWidget::set_scroll_value(int value)
{
    m_scroll_area->verticalScrollBar()->setValue(value);
}

void TimelineConrolWidget::update_timeline_mark()
{
    const int handle_width = TimelineTheme::range_handle_width();
    int currentFrame = m_timeline->current_frame();
    int start = m_timeline->range_start();
    int end = m_timeline->range_end();
    if ( currentFrame >= start && currentFrame <= end)
    {
        m_timeline_marker_line->show();
        int mark_x = handle_width + (currentFrame - start)*(width() - 2*handle_width)/(end - start);
        int width = TimelineTheme::timeline_marker_line_width();
        int ruler_height = TimelineTheme::ruler_height();
        m_timeline_marker_line->setGeometry(mark_x - width/2, ruler_height + TimelineTheme::visible_range_row_height(), width, height());
    }
    else
    {
        m_timeline_marker_line->hide();
    }
}

void TimelineConrolWidget::update_timeline_play_mark()
{
    const int handle_width = TimelineTheme::range_handle_width();
    int effective_width = width() - 2*handle_width;
    int range_start = m_timeline->range_start();
    int range_duration = m_timeline->range_duration();
    int line_x = ((m_timeline->play_frame() - range_start)*effective_width)/range_duration + handle_width;

    if (line_x >= 0 and line_x < width())
    {
        int width = TimelineTheme::timeline_marker_line_width();
        m_timeline_play_marker_line->setGeometry(line_x - width/2, 0, width, height());
        m_timeline_play_marker_line->show();
    }
    else
    {
        m_timeline_play_marker_line->hide();
    }
}
