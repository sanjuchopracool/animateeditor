#ifndef PROPERTY_EDITOR_ITEM_H
#define PROPERTY_EDITOR_ITEM_H

#include <QWidget>
#include <property_type.h>

namespace Ui {
class PropertyEditorItem;
}

class QVBoxLayout;
class INode;
class ClickableLabel;
class PropertyEditorItem : public QWidget
{
    Q_OBJECT

public:
    explicit PropertyEditorItem(INode* node,
                                const QString& name,
                                PropertyType type,
                                QWidget *editor,
                                QWidget *parent = nullptr);
    ~PropertyEditorItem();

    void add_sub_item(PropertyEditorItem * child);
    void set_lock(bool lock);
    int items_count() const;
    int min_width() const;

    void update_theme();
    void set_left_indent(int indent);
    int left_indent() const;

signals:
    void expand(bool);
    void animation_state_changed(bool);
    void add_keyframe_clicked();
private:
    void set_editor_widget(QWidget *editor);
    void copy_animations(QDataStream& out) const;
    void paste_animations(QDataStream& in_stream);

protected:
    void showEvent(QShowEvent *event);
    void keyPressEvent(QKeyEvent *event);

private slots:
    void slot_expand(bool checked);
    void slot_animate(bool animate);
    void copy_animations();
    void paste_animations();

private:
    Ui::PropertyEditorItem *m_ui;
    INode * m_node = nullptr;
    PropertyType m_type;
    QVBoxLayout *m_property_item_layout = nullptr;
    QList<PropertyEditorItem*> m_sub_items;
    QWidget* m_editor = nullptr;
    ClickableLabel * m_add_keyframe_label = nullptr;
};

#endif // PROPERTY_EDITOR_ITEM_H
