#ifndef TIMELINE_EDITOR_WIDGET_H
#define TIMELINE_EDITOR_WIDGET_H

#include <QFrame>

class QVBoxLayout;
class QScrollArea;

class TimelineEditorWidget : public QFrame
{
    Q_OBJECT
public:
    explicit TimelineEditorWidget(QWidget *parent = nullptr);
    ~TimelineEditorWidget();

    void update_theme();

    void add_widget(QWidget *item);
    void remove_widget(QWidget *item);

    void set_scroll_value(int value);
signals:
    void scroll_value_changed(int value);

public slots:

private:
    QVBoxLayout * m_property_item_layout = nullptr;
    QWidget * m_main_widget = nullptr;
    QScrollArea * m_scroll_area = nullptr;

};

#endif // TIMELINE_EDITOR_WIDGET_H
