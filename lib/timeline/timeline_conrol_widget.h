#ifndef TIMELINE_CONROL_WIDGET_H
#define TIMELINE_CONROL_WIDGET_H

#include <QFrame>

class QVBoxLayout;
class QScrollArea;
class TimeLine;
class TimelineItemControlWidget;
class QSlider;
class TimelineRangeSelector;

class TimelineConrolWidget : public QFrame
{
    Q_OBJECT
public:
    explicit TimelineConrolWidget(TimeLine *timeline, QWidget *parent = nullptr);
    ~TimelineConrolWidget();

    void add_item(TimelineItemControlWidget * item);
    void update_theme();

    void set_scroll_value(int value);
signals:
    void scroll_value_changed(int value);

public slots:
    void update_timeline_mark();
    void update_timeline_play_mark();

private:
    class TimeLine* m_timeline;
    QVBoxLayout * m_timeline_item_layout = nullptr;
    QWidget * m_timeline_marker_line = nullptr;
    QWidget * m_timeline_play_marker_line = nullptr;
    TimelineRangeSelector * m_range_widget = nullptr;
    QScrollArea * m_scroll_area;
    QWidget * m_main_widget= nullptr;
};

#endif // TIMELINE_CONROL_WIDGET_H
