#ifndef KEYFRAMES_WIDGET_H
#define KEYFRAMES_WIDGET_H

#include "property_type.h"

#include <QWidget>

class INode;
class QLabel;
class KeyFrame;
class TimeLine;

class KeyFramesWidget : public QWidget
{
    Q_OBJECT
public:
    KeyFramesWidget(INode* node,
                    PropertyType type,
                    TimeLine * timeline,
                    QWidget *parent = nullptr);

public slots:
    void force_frames_update();
    void add_key_frame();
    void slot_value_edited();

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

    void paintEvent(QPaintEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void focusOutEvent(QFocusEvent *event) override;

private:
    QList<KeyFrame*> m_keyframes;
    QList<KeyFrame*> m_selected_frames;
    INode* m_node = nullptr;
    PropertyType m_type;
    TimeLine * m_timeline;
    QPixmap m_keyframe_pix;
    QPixmap m_keyframe_selected_pix;
    bool m_dirty = true;
    bool m_is_not_group = false;
    int m_click_x = 0;
};

#endif // KEYFRAMES_WIDGET_H
