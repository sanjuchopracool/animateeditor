#ifndef THEME_H
#define THEME_H

#include <QColor>

class TimelineTheme
{
public:
    static int row_height();
    static int row_line_width();

    static const QColor &property_background_color();
    static const QColor &property_line_color();

    static const QColor &foreground_color();
    static const QColor &editor_text_color();
    static const QColor &disabled_text_color();

    static const QColor &timecontrol_background_color();
    static const QColor &timecontrol_line_color();

    static const QColor &timecontrol_line_marker_color();
    static int timeline_marker_line_width();

    static const QColor &ruler_color();
    static const QColor &ruler_text_color();
    static int ruler_height();
    static int min_scale_unit_width();

    // Timeline range, height will be same as row_height
    static int range_handle_width();
    static int visible_range_row_height();

    static QColor play_range_color();
    static QColor range_handle_color();
    static QColor range_color();
    static QColor play_range_line_color();
};

#endif // THEME_H
