#include "timeline_drag_item.h"
#include "ui_timeline_drag_item.h"

#include "timeline_theme.h"
#include "timeline_play_range_selector.h"
#include "timeline.h"

#include <inode.h>

#include <QMouseEvent>
#include <QApplication>

#include <QPainter>
#include <QStyleOption>


TimelineDragItem::TimelineDragItem(INode *node,
                                   TimeLine * timeline,
                                   QWidget *parent) :
    QFrame(parent),
    ui(new Ui::TimelineDragItem),
    m_node(node),
    m_timeline(timeline),
    m_drag_icon("://timeline/drag.png")
{
    ui->setupUi(this);
    int row_height = TimelineTheme::row_height();
    m_icon_rect = QRect(0, 0, row_height, row_height);
}

TimelineDragItem::~TimelineDragItem()
{
    delete ui;
}

void TimelineDragItem::set_color(const QColor &color)
{
    m_color = color;
}

void TimelineDragItem::mousePressEvent(QMouseEvent *event)
{
    m_old_x = event->pos().x();
    if (is_inside_item_range(m_old_x))
    {
        m_timeline_playing = (m_timeline->state() == QTimeLine::Running);
        if (m_timeline_playing)
            m_timeline->stop();

        m_dragging = true;
        QApplication::setOverrideCursor(Qt::ClosedHandCursor);
        int row_height = TimelineTheme::row_height();
        m_icon_rect.moveLeft(m_old_x -  row_height/2);
        event->accept();
        update();
    }
    else
        QWidget::mousePressEvent(event);
}

void TimelineDragItem::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_dragging)
    {
        m_dragging = false;
        if (m_timeline_playing)
            m_timeline->resume();

        QApplication::restoreOverrideCursor();
        event->accept();
        update();
    }
    else
        QWidget::mouseReleaseEvent(event);
}

void TimelineDragItem::mouseMoveEvent(QMouseEvent *event)
{
    if (m_dragging)
    {
        int new_x = event->pos().x();
        int delta_frames = ((new_x - m_old_x)*m_timeline->range_duration())
                /(width() - 2*TimelineTheme::range_handle_width());
        if (delta_frames)
        {
            m_node->move_time_range(delta_frames);

            if (m_timeline_playing)
                m_node->update_animation(m_timeline->play_frame());
            else
                m_node->update_animation(m_timeline->current_frame());

            m_icon_rect.moveLeft(m_old_x -  height()/2);
            update();
            emit node_animation_time_changed();

            m_old_x = new_x;
            event->accept();
        }
    }
    else
        QWidget::mouseMoveEvent(event);
}

void TimelineDragItem::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);

    auto range_color = TimelineTheme::play_range_color();
    range_color.setAlpha(75);
    TimeLinePlayRangeSelector::fill_play_range(m_timeline, size(), &painter, range_color);

    const int handle_width = TimelineTheme::range_handle_width();
    int effective_width = width() - 2*handle_width;
    int item_start = m_node->start_time();
    int item_end = m_node->end_time();

    int range_start = m_timeline->range_start();
    int range_duration = m_timeline->range_duration();

    KeyFrameTime max_end = (handle_width*range_duration)/effective_width + m_timeline->range_end();;
    if (item_end > max_end)
        item_end = max_end;

    int item_start_x = ((item_start - range_start)*(effective_width)/range_duration) + handle_width;
    int item_end_x = ((item_end - range_start)*(effective_width)/range_duration) + handle_width;

    if (item_start_x < 0)
        item_start_x = 0;

    if (item_end_x > width())
        item_end_x = width();

    painter.fillRect(QRect(item_start_x, 0, item_end_x - item_start_x, height()), m_color);

    if (m_dragging)
    {
        painter.drawPixmap(m_icon_rect, m_drag_icon);
    }
}

bool TimelineDragItem::is_inside_item_range(int x) const
{
    const int handle_width = TimelineTheme::range_handle_width();
    x -= handle_width;
    long effective_width = width() - 2*handle_width;

    int item_start = m_node->start_time();
    int item_end = m_node->end_time();
    int range_start = m_timeline->range_start();
    int range_duration = m_timeline->range_duration();

    KeyFrameTime max_end = (handle_width*range_duration)/effective_width + m_timeline->range_end();;
    if (item_end > max_end)
        item_end = max_end;

    int item_start_x = ((item_start - range_start)*effective_width)/range_duration;
    int item_end_x = ((item_end - range_start)*effective_width)/range_duration;

    return (x >= item_start_x and x <= item_end_x);
}
