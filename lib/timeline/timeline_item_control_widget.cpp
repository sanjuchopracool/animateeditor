#include "timeline_item_control_widget.h"

#include "inode.h"
#include "timeline_theme.h"
#include "keyframe_container_widget.h"
#include "timeline_drag_item.h"
#include "timeline.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPainter>
#include <QStyleOption>
#include <QMouseEvent>

namespace
{
const QString style_sheet("background-color: %1;"
                          "color: %2;");
}

TimelineItemControlWidget::TimelineItemControlWidget(INode *node, TimeLine *timeline, QWidget *parent) :
    QWidget(parent),
    m_node(node),
    m_timeline(timeline)
{
    m_drag_item = new TimelineDragItem(node, timeline);

    m_child_items_widget = new QWidget;
    m_sub_items_layout = new QVBoxLayout;
    m_sub_items_layout->setMargin(0);
    m_sub_items_layout->setSpacing(0);
    m_child_items_widget->setLayout(m_sub_items_layout);

    QVBoxLayout * layout = new QVBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(m_drag_item);
    layout->addWidget(m_child_items_widget);
    setLayout(layout);

    set_color("#339266");

    connect(m_timeline, SIGNAL(range_changed()),
            this, SLOT(force_frames_update()));
    connect(m_timeline, SIGNAL(play_range_changed()),
            this, SLOT(force_frames_update()));

    connect(m_drag_item, &TimelineDragItem::node_animation_time_changed,
            this, [=]()
    {
        for (auto item : m_sub_items)
            item->force_frames_update();
    });
}

TimelineItemControlWidget::~TimelineItemControlWidget()
{
}

void TimelineItemControlWidget::add_key_frame_widget(KeyFrameContainerWidget *item)
{
    if (item)
    {
        m_sub_items_layout->addWidget(item);
        m_sub_items.append(item);
    }
}

void TimelineItemControlWidget::update_theme()
{
    setStyleSheet(style_sheet.
                  arg(TimelineTheme::timecontrol_background_color().name()).
                  arg(TimelineTheme::foreground_color().name()));

    m_child_items_widget->setStyleSheet(style_sheet.
                                        arg(TimelineTheme::timecontrol_line_color().name()).
                                        arg(TimelineTheme::foreground_color().name()));

    unsigned line_width = TimelineTheme::row_line_width();
    m_sub_items_layout->setContentsMargins(0, 0, 0, 0);
    m_sub_items_layout->setSpacing(line_width);
    layout()->setSpacing(line_width);
    m_drag_item->setFixedHeight(TimelineTheme::row_height());
    for ( auto const item : m_sub_items)
    {
        item->update_theme();
    }
}

void TimelineItemControlWidget::set_color(const QColor &color)
{
    m_drag_item->set_color(color);
}

void TimelineItemControlWidget::expand_sub_items(bool expand)
{
    m_child_items_widget->setVisible(expand);
}

void TimelineItemControlWidget::force_frames_update()
{
    m_drag_item->update();
    for (auto item : m_sub_items)
        item->force_frames_update();
}
