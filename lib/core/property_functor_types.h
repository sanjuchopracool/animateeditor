#ifndef PROPERTY_FUNCTOR_TYPES_H
#define PROPERTY_FUNCTOR_TYPES_H


template <typename Derived, typename PropertyType>
using ref_setter_func = void (Derived::*)(const PropertyType &);

template <typename Derived, typename PropertyType>
using ref_getter_func = const PropertyType& (Derived::*)() const;

template <typename Derived, typename PropertyType>
using value_setter_func = void (Derived::*)(PropertyType);

template <typename Derived, typename PropertyType>
using value_getter_func = PropertyType (Derived::*)() const;

template <typename Derived, typename PropertyType>
struct SetterTypeDef
{
    using type = ref_setter_func<Derived, PropertyType>;
};

template <typename Derived, typename PropertyType>
struct GetterTypeDef
{
    using type = ref_getter_func<Derived, PropertyType>;
};


///////////////// bool specialization //////////////////////////////
template <typename Derived>
struct SetterTypeDef<Derived, bool>
{
    using type = value_setter_func<Derived, bool>;
};

template <typename Derived>
struct GetterTypeDef<Derived, bool>
{
    using type = value_getter_func<Derived, bool>;
};
/////////////////////////////////////////////////////////////////////

template <typename Derived, typename PropertyType>
using setter_func_type = typename SetterTypeDef<Derived, PropertyType>::type;

template <typename Derived, typename PropertyType>
using getter_func_type = typename GetterTypeDef<Derived, PropertyType>::type;

#endif // PROPERTY_FUNCTOR_TYPES_H
