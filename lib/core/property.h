#ifndef PROPERTY_H
#define PROPERTY_H

#include <property_type.h>
#include <QList>

class Property
{
public:
    Property(PropertyType type = PropertyType::None)
        : m_type(type)
    {

    }

    ~Property()
    {
        if (m_parent)
            m_parent->remove_sub_property(this);

        qDeleteAll(m_sub_properties);
    }

    inline PropertyType type() const {
        return m_type;
    }

    void add_sub_property(Property* property) {
        if (not m_sub_properties.contains(property))
        {
            property->set_parent(this);
            m_sub_properties.append(property);
        }
    }

    void remove_sub_property(Property* property) {
        if (property)
        {
            property->set_parent(nullptr);
            m_sub_properties.removeOne(property);
        }
    }

    const QList<const Property*>& sub_properties() const {
        return m_sub_properties;
    }

private:
    inline void set_parent(Property *parent) {
            m_parent = parent;
    }

private:
    PropertyType m_type = PropertyType::None;
    Property *m_parent = nullptr;
    QList<const Property*> m_sub_properties;
};

#endif // PROPERTY_H
