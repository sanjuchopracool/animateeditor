
INCLUDEPATH +=$$PWD

HEADERS += \
    $$PWD/inode.h \
    $$PWD/property_functor.h \
    $$PWD/iserialize.h \
    $$PWD/ianimation_support.h \
    $$PWD/property_type.h \
    $$PWD/keyframe.h \
    $$PWD/easing_types.h \
    $$PWD/node_types.h \
    $$PWD/abstract_node.h \
    $$PWD/animation_phrase.h \
    $$PWD/interpolation.h \
    $$PWD/Easing.h \
    $$PWD/property_interface.h \
    $$PWD/property.h \
    $$PWD/property_functor_types.h \
    $$PWD/composition.h \
    $$PWD/inode_factory.h \
    $$PWD/core_node_factory.h \
    $$PWD/static_memory_cleaner.h \
    $$PWD/property_groups_tree.h \
    $$PWD/rectangle.h

SOURCES += \
    $$PWD/ianimation_support.cpp \
    $$PWD/keyframe.cpp \
    $$PWD/animation_phrase.cpp \
    $$PWD/abstract_node.cpp \
    $$PWD/composition.cpp \
    $$PWD/inode.cpp \
    $$PWD/core_node_factory.cpp \
    $$PWD/static_memory_cleaner.cpp \
    $$PWD/property_groups_tree.cpp \
    $$PWD/rectangle.cpp
