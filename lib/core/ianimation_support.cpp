#include "ianimation_support.h"
#include "animation_phrase.h"

#include <algorithm>
#include <set>
#include <QDataStream>

IAnimationSupport::IAnimationSupport()
{

}

IAnimationSupport::~IAnimationSupport()
{
    for(auto& info : m_animation_info)
    {
        delete info;
    }
}

bool IAnimationSupport::is_animating(PropertyType type) const
{
    auto info = m_animation_info.value(type, nullptr);
    if (info)
        return info->is_animating;

    return false;
}

void IAnimationSupport::animate_property(PropertyType type, bool animate)
{
    auto info = m_animation_info.value(type, nullptr);
    if (not info)
    {
        m_animation_info.insert(type, new PropertyAnimationInfo);
    }
    m_animation_info[type]->is_animating = animate;
}

QList<KeyFrame *> IAnimationSupport::keyframes(PropertyType type) const
{
    auto info = m_animation_info.value(type, nullptr);
    if (info)
        return info->keyframes;

    return QList<KeyFrame *>();
}

void IAnimationSupport::clear_keyframes(PropertyType type)
{
    if (m_animation_info.contains(type))
    {
        auto frames = m_animation_info[type]->keyframes;
        for (int i = (frames.count() - 1); i >= 0; --i)
            remove_keyframe(type, frames[i]);
    }
}

KeyFrame *IAnimationSupport::add_keyframe(PropertyType type, KeyFrameTime t, const QVariant &val)
{
    KeyFrame * result = nullptr;

    if(not m_animation_info.contains(type))
    {
        m_animation_info.insert(type, new PropertyAnimationInfo);
    }

    auto &frames = m_animation_info[type]->keyframes;
    if (frames.size())
    {
        auto begin = frames.begin();
        auto end = frames.end();

        auto found = std::find_if(begin, end,
                                  [=](KeyFrame* frame)
        {
            return frame->time() == t;
        });

        if (found == end)
        {
            result = new KeyFrame(t,val);

            // find correct index to insert
            auto insert_at = std::find_if(begin, end,
                                          [=](KeyFrame* frame)
            {
                return *frame > *result;
            });

            frames.insert(insert_at, result);
        }
    }
    else
    {
        result = new KeyFrame(t,val);
        m_animation_info[type]->keyframes.append(result);
    }

    on_frame_added(type, result);
    return result;
}

void IAnimationSupport::remove_keyframe(PropertyType type, KeyFrame *keyframe)
{
    if ( keyframe && m_animation_info.contains(type))
    {
        auto &frames = m_animation_info[type]->keyframes;
        int remove_index = frames.indexOf(keyframe);
        if (-1 != remove_index)
        {
            auto &phrases = m_animation_info[type]->phrases;
            bool is_last_frame = (remove_index == (frames.size() - 1));
            if (phrases.size() > remove_index)
            {
                if (1 == frames.size())
                {
                    // its the only frame
                    delete phrases.first();
                    phrases.clear();
                }
                else
                {
                    // there are at least 2 frames
                    if (0 == remove_index)
                    {
                        auto phrase = phrases.first();
                        auto next_phrase = phrases.at(1);

                        next_phrase->set_start_frame(phrase->start_frame());
                        next_phrase->set_start_value(phrase->start_value());

                        phrases.removeFirst();
                        delete phrase;
                    }
                    else if (is_last_frame)
                    {
                        auto phrase = phrases.last();
                        phrases.removeLast();
                        delete phrase;
                    }
                    else
                    {
                        auto phrase = phrases.at(remove_index);
                        auto previous_phrase = phrases.at(remove_index - 1);
                        auto next_phrase = phrases.at(remove_index + 1);

                        next_phrase->set_start_frame(previous_phrase->end_frame());
                        phrases.removeAt(remove_index);
                        delete phrase;
                    }
                }
            }

            frames.removeAt(remove_index);
            delete keyframe;
        }
    }
}

KeyFrame *IAnimationSupport::update_keyframe(PropertyType type,
                                             KeyFrame *frame,
                                             KeyFrameTime t, bool force_update)
{
    KeyFrame *result = frame;
    if ( frame and (frame->time() != t) and m_animation_info.contains(type))
    {
        auto &frames = m_animation_info[type]->keyframes;
        int index = frames.indexOf(frame);
        if (-1 != index)
        {
            if (not force_update)
            {
                if (t < frame->time())
                {
                    if (index > 0)
                    {
                        KeyFrame * left = frames.at(index - 1);
                        if (t >= left->time())
                            force_update = true;
                    }
                    else
                        force_update = true;
                }
                else
                {
                    if (index < (frames.size() - 1))
                    {
                        KeyFrame * right = frames.at(index+1);
                        if (t <= right->time())
                            force_update = true;

                    }
                    else
                        force_update = true;
                }
            }

            if (force_update)
                frame->set_time(t);
            else
            {
                // delete the frame and add new frame
                QVariant value = frame->value();
                remove_keyframe(type, frame);
                result = add_keyframe(type, t, value);
            }
        }
    }

    return result;
}

void IAnimationSupport::update_keyframe_value(PropertyType type, KeyFrameTime t, const QVariant &val)
{
    if (m_animation_info.contains(type))
    {
        auto &frames = m_animation_info[type]->keyframes;
        if (frames.size())
        {
            auto begin = frames.begin();
            auto end = frames.end();

            auto found = std::find_if(begin, end,
                                      [=](KeyFrame* frame)
            {
                return frame->time() == t;
            });

            if (found != end)
            {
                (*found)->set_value(val);
            }
        }
    }
}

void IAnimationSupport::set_start_time(KeyFrameTime start)
{
    if(start >= KeyFrameMinTime && start < m_end_time)
        m_start_time = start;
}

void IAnimationSupport::move_time_range(KeyFrameTime t)
{
    set_start_time(m_start_time + t);
    set_end_time(m_end_time + t);

    for (auto type : m_animation_info.keys() )
    {
        PropertyAnimationInfo * info = m_animation_info.value(type, nullptr);
        if (info)
        {
            for(auto frame : info->keyframes)
                update_keyframe(type, frame, t + frame->time(), true);
        }
    }
}

void IAnimationSupport::set_end_time(KeyFrameTime end)
{
    if (end <= KeyFrameMaxTime && end > m_start_time)
        m_end_time = end;
}

KeyFrameTime IAnimationSupport::start_time() const
{
    return m_start_time;
}

KeyFrameTime IAnimationSupport::end_time() const
{
    return m_end_time;
}

bool IAnimationSupport::save_animations(QDataStream &out) const
{
    out << m_start_time;
    out << m_end_time;

    auto keys = m_animation_info.keys();
    int count =  keys.count();
    out << count;

    for( const auto& key : keys)
    {
        const auto& info = m_animation_info[key];
        out << key;
        out << info->is_animating;
        out << info->keyframes.size();
        for(const auto& keyframe : info->keyframes )
        {
            out << keyframe->time();
            out << keyframe->value();
        }
    }

    return true;
}

bool IAnimationSupport::load_animations(QDataStream &in)
{
    in >> m_start_time;
    in >> m_end_time;
    int count = 0;
    in >> count;

    for( int i = 0; i < count; ++i)
    {
        PropertyType type;
        in >> type;

        m_animation_info.insert(type, new PropertyAnimationInfo);
        PropertyAnimationInfo* info = m_animation_info[type];
        in >> info->is_animating;

        int no_of_keyframes = 0;
        in >> no_of_keyframes;
        for( int i = 0; i < no_of_keyframes; ++i)
        {
            KeyFrameTime time = 0;
            in >> time;
            QVariant value;
            in >> value;
            add_keyframe(type, time, value);
        }
    }

    return true;
}

void IAnimationSupport::on_frame_added(PropertyType type, KeyFrame *frame)
{
    if (frame && m_animation_info[type])
    {
        AnimationPhrase *phrase = nullptr;
        auto &frames = m_animation_info[type]->keyframes;
        auto &phrases = m_animation_info[type]->phrases;

        // if its the only frame
        if (1 == frames.size())
        {
            phrase = new AnimationPhrase(nullptr, frame);
            phrase->set_start_value(property_value(type));
            phrases.append(phrase);
        }
        else
        {
            int frame_index = frames.indexOf(frame);
            if (-1 != frame_index && phrases.size())
            {
                // index will not be zero, above case handles it
                // items will be more than 2 and atleast one frame should exist

                // if its the first frame
                bool is_last_frame = (frame_index == (frames.count() -1 ));
                if (0 == frame_index)
                {
                    auto first_phrase = phrases.first();
                    QVariant start_val = first_phrase->start_value();
                    first_phrase->set_start_frame(frame);

                    phrase = new AnimationPhrase(nullptr, frame);
                    phrase->set_start_value(start_val);
                    phrases.prepend(phrase);
                }
                else if (is_last_frame)
                {
                    auto last_phrase = phrases.last();
                    phrase = new AnimationPhrase(last_phrase->end_frame(), frame);
                    phrases.append(phrase);
                }
                else
                {
                    // keyframe is inserted in between frames
                    auto phrase_before = phrases.at(frame_index - 1);

                    auto phrase_after = phrases.at(frame_index);
                    phrase_after->set_start_frame(frame);

                    phrase = new AnimationPhrase(phrase_before->end_frame(), frame);
                    phrases.insert(frame_index, phrase);
                }
            }
        }

        if (phrase)
            phrase->set_lerp(lerp_for_type(type));
    }
}
