#ifndef NODE_TYPES_H
#define NODE_TYPES_H

#include <QDataStream>

enum class NodeType
{
    Composition = 0,
    Abstract,
    Rectangle,
    LastNodeType,
};


inline QDataStream & operator<<(QDataStream &out, NodeType type) {
    out << static_cast<unsigned>(type);
    return out;
}

inline QDataStream & operator>>(QDataStream &in, NodeType& node_type) {
    unsigned type;
    in >> type;

    node_type = static_cast<NodeType>(type);
    return in;
}

#endif // NODE_TYPES_H
