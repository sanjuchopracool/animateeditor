#include "core_node_factory.h"

#include "abstract_node.h"
#include "rectangle.h"

INode *CoreNodeFactory::create_node(NodeType type)
{
    INode * result = nullptr;
    switch (type) {
    case NodeType::Abstract:
        result = new AbstractNode;
        break;
    case NodeType::Rectangle:
        result = new Rectangle;
        break;
    default:
        break;
    }

    return result;
}
