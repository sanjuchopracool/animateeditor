#include "keyframe.h"
#include <animation_phrase.h>

struct KeyFrameData
{
    KeyFrameTime t = {};
    KeyFrameEaseType ease_type = KeyFrameEaseType::None;
    QVariant value;

    AnimationPhrase *left_phrase = nullptr;
    AnimationPhrase *right_phrase = nullptr;
};

KeyFrame::KeyFrame(KeyFrameTime t, const QVariant &value, KeyFrameEaseType type)
    : d (std::make_unique<KeyFrameData>())
{
    d->t = t;
    d->ease_type = type;
    d->value = value;
}

KeyFrame::~KeyFrame()
{
}

void KeyFrame::set_time(KeyFrameTime t)
{
    d->t = t;
    update_phrases();
}

void KeyFrame::update_phrases()
{
    if (d->left_phrase)
        d->left_phrase->update_lerp();

    if (d->right_phrase)
        d->right_phrase->update_lerp();
}

KeyFrameTime KeyFrame::time() const
{
    return d->t;
}

void KeyFrame::add_easing(KeyFrameEaseType type)
{
    if (KeyFrameEaseType::InOut != d->ease_type)
    {
        KeyFrameEaseType new_value = static_cast<KeyFrameEaseType>(d->ease_type | type);
        if ( new_value != d->ease_type)
        {
            d->ease_type = new_value;
        }
    }
}

KeyFrameEaseType KeyFrame::ease_type() const
{
    return d->ease_type;
}

void KeyFrame::set_easing(KeyFrameEaseType type)
{
    if (d->ease_type != type)
    {
        d->ease_type = type;
    }
}

QVariant KeyFrame::value() const
{
    return d->value;
}

void KeyFrame::set_value(const QVariant &value)
{
    d->value = value;
    update_phrases();
}

void KeyFrame::set_left_phrase(AnimationPhrase *phrase)
{
    d->left_phrase = phrase;
}

void KeyFrame::set_right_phrase(AnimationPhrase *phrase)
{
    d->right_phrase = phrase;
}
