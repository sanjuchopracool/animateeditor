#ifndef CORE_NODE_FACTORY_H
#define CORE_NODE_FACTORY_H

#include "inode_factory.h"

class CoreNodeFactory : public INodeFactory
{
public:
    INode *create_node(NodeType type) override;
};

#endif // CORE_NODE_FACTORY_H
