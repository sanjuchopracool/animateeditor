#ifndef PROPERTY_FUNCTOR_H
#define PROPERTY_FUNCTOR_H

#include "property_functor_types.h"
#include "interpolation.h"
#include "property_type.h"

#include <QVariant>
#include <QPointF>
#include <memory>

class INode;
class QWidget;
class Interpolate;

using PropertySetterInterface = std::function<void( INode* obj, const QVariant& value)>;
using PropertyGetterInterface = std::function<QVariant(const INode* obj)>;
using EditorUpdateInterface = std::function<void(const QVariant&)>;
using CustomGraphicsUpdater = std::function<void( INode* obj, PropertyType type, const QVariant &value)>;

template <typename Derived>
class GraphicsUpdateTemplate
{
public:

    using update_func = void (Derived::*)(PropertyType type, const QVariant &value);
    GraphicsUpdateTemplate(update_func func)
        : m_func(func) {}

    void operator()( INode* obj, PropertyType type, const QVariant &value)
    {
        if(m_func)
            (static_cast<Derived*>(obj)->*m_func)(type, value);
    }

private:
    update_func m_func = nullptr;
};

class PropertyFunctor
{
public:
    PropertyFunctor(PropertySetterInterface setter = nullptr,
                    PropertyGetterInterface getter = nullptr,
                    std::unique_ptr<Interpolate>  interpolate = nullptr)
        : m_setter(setter),
          m_getter(getter),
          m_interpolate(std::move(interpolate))
    {

    }

    virtual ~PropertyFunctor () {}
    PropertySetterInterface m_setter = nullptr;
    PropertyGetterInterface m_getter = nullptr;
    std::unique_ptr<Interpolate> m_interpolate;
};

template <typename Derived, typename PropertyType>
class PropertySetterTemplate
{
public:
    PropertySetterTemplate(setter_func_type<Derived, PropertyType> func)
        : m_func(func) {}

    void operator()( INode* obj, const QVariant& var)
    {
        if(m_func)
            (static_cast<Derived*>(obj)->*m_func)(var.value<PropertyType>());
    }

private:
    setter_func_type<Derived, PropertyType> m_func = nullptr;
};

template <typename Derived, typename PropertyType>
class PropertyGetterTemplate
{
public:
    PropertyGetterTemplate(getter_func_type<Derived, PropertyType> func)
        : m_func(func) {}

    QVariant operator()(const INode* obj)
    {
        if (m_func)
            return (static_cast<const Derived*>(obj)->*m_func)();
        return QVariant();
    }

private:
    getter_func_type<Derived, PropertyType> m_func = nullptr;
};

template <typename Derived>
class PropertyUpdateTemplate
{
public:
    using update_func = void (Derived::*)(const QVariant &);
    PropertyUpdateTemplate(QWidget* editor, update_func func)
        : m_func(func),
          m_editor(editor)
    {}

    void operator()(const QVariant& val)
    {
        if (m_func)
        {
            QSignalBlocker blocker(m_editor);
            (static_cast<Derived*>(m_editor)->*m_func)(val);
        }
    }

private:
    update_func m_func = nullptr;
    QWidget* m_editor = nullptr;
};

template <typename Derived, typename PropertyType>
class PropertyFunctorTemplate : public PropertyFunctor
{
public:
    PropertyFunctorTemplate(setter_func_type<Derived, PropertyType> setter,
                            getter_func_type<Derived, PropertyType> getter)
        : PropertyFunctor(PropertySetterTemplate<Derived, PropertyType>(setter),
                          PropertyGetterTemplate<Derived, PropertyType>(getter),
                          std::make_unique<PropertyInterpolation<Derived, PropertyType>>(setter))
    {

    }
};

template <typename Derived>
using DoubleFunctor = PropertyFunctorTemplate<Derived, double>;

template <typename Derived>
using PointFFunctor = PropertyFunctorTemplate<Derived, QPointF>;

template <typename Derived>
using BoolFunctor = PropertyFunctorTemplate<Derived, bool>;


#endif // PROPERTY_FUNCTOR_H
