#ifndef IPROPERTY_INTERFACE_H
#define IPROPERTY_INTERFACE_H

#include <QHash>
#include "property_type.h"
#include "property_functor.h"

class Property;
class QWidget;

class IPropertyInterface
{
public:
    virtual ~IPropertyInterface() {}
    virtual const QList<const Property*>&  properties() = 0;
    virtual PropertyFunctor *property_functors(PropertyType type) const = 0;

    void set_editor(PropertyType type, EditorUpdateInterface update_func)
    {
        m_editors.insert(type, update_func);
    }

    void unset_editor(PropertyType type)
    {
        m_editors.remove(type);
    }

    EditorUpdateInterface get_editor(PropertyType type) const
    {
        return m_editors.value(type, nullptr);
    }

protected:
    virtual void register_property_functors(PropertyType type,
                                            PropertyFunctor *interface) = 0;
    QHash<const PropertyType, EditorUpdateInterface> m_editors;
};

#endif // IPROPERTY_INTERFACE_H
