#ifndef ISERIALIZE_H
#define ISERIALIZE_H

class QDataStream;
class Composition;
class ISerialize
{
public:
    virtual ~ISerialize() {}

    virtual bool save(QDataStream& out) const = 0;
    virtual bool load(QDataStream& in) = 0;
};

#endif // ISERIALIZE_H
