#ifndef NODE_FACTORY_H
#define NODE_FACTORY_H

#include "node_types.h"

class INode;

class INodeFactory
{
public:
    virtual INode *create_node(NodeType type) = 0;
    virtual ~INodeFactory() {}
};

#endif // NODE_FACTORY_H
