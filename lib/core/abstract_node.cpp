#include "abstract_node.h"
#include "interpolation.h"
#include "property_groups_tree.h"

#include <QVersionNumber>
#include <QDataStream>

namespace
{
    QVersionNumber s_version(1, 0, 0);
    QList<const Property*> m_basic_properties;
    QMap<PropertyType, PropertyFunctor*> m_basic_func_map;
}



AbstractNode::AbstractNode()
{
    set_node_type(NodeType::Abstract);
}

AbstractNode::~AbstractNode()
{
}

bool AbstractNode::save(QDataStream &out) const
{
    out << s_version;

    out << m_pos;
    out << m_anchor_pos;
    out << m_scale;
    out << m_opacity;
    out << m_rotation;
    out << m_is_visible;
    out << m_name;
    out << m_id;

    return true;
}

bool AbstractNode::load(QDataStream &in)
{
    QVersionNumber version;
    in >> version;

    QPointF pointf_val;

    in >> pointf_val;
    set_position(pointf_val);

    in >> pointf_val;
    set_anchor_position(pointf_val);

    in >> pointf_val;
    set_scale(pointf_val);

    qreal double_val;

    in >> double_val;
    set_opacity(double_val);

    in >> double_val;
    set_rotation(double_val);

    bool bool_val;
    in >> bool_val;
    set_visible(bool_val);

    QString name;
    in >> name;
    set_name(name);

    int id;
    in >> id;
    set_id(id);

    return true;
}

QList<const Property *> &AbstractNode::properties()
{
    if (m_basic_properties.isEmpty())
    {
        m_basic_properties.append(PropertyGroupsTree::tranform_group());

        PropertyFunctor *opacity_functor = new DoubleFunctor<INode>(&INode::set_opacity,
                                                                    &INode::get_opacity);
        register_property_functors(PropertyType::Opacity, opacity_functor);

        PropertyFunctor *scale_functor = new PointFFunctor<INode>(&INode::set_scale,
                                                                  &INode::get_scale);
        register_property_functors(PropertyType::Scale, scale_functor);

        PropertyFunctor *rotation_functor = new DoubleFunctor<INode>(&INode::set_rotation,
                                                                     &INode::get_rotation);
        register_property_functors(PropertyType::Rotation, rotation_functor);

        PropertyFunctor *visible_functor = new BoolFunctor<INode>(&INode::set_visible,
                                                                  &INode::is_visible);
        register_property_functors(PropertyType::Visible, visible_functor);

        PropertyFunctor *position_functor = new PointFFunctor<INode>(&INode::set_position,
                                                                     &INode::position);
        register_property_functors(PropertyType::Position, position_functor);

        PropertyFunctor *anchor_functor = new PointFFunctor<INode>(&INode::set_anchor_position,
                                                                   &INode::anchor_position);
        register_property_functors(PropertyType::AnchorPosition, anchor_functor);
    }

    return  m_basic_properties;
}

void AbstractNode::register_property_functors(PropertyType type, PropertyFunctor *interface)
{
    m_basic_func_map.insert(type, interface);
}

PropertyFunctor *AbstractNode::property_functors(PropertyType type) const
{
    return m_basic_func_map.value(type, nullptr);
}

std::unique_ptr<Interpolate> AbstractNode::lerp_for_type(PropertyType type)
{
    auto functor = property_functors(type);
    if (functor)
        return functor->m_interpolate->clone();

    return nullptr;
}

void AbstractNode::set_graphics_updater(CustomGraphicsUpdater updater)
{
    m_graphics_updater = updater;
}

void AbstractNode::clean_static_memory()
{
    m_basic_properties.clear();

    qDeleteAll(m_basic_func_map.values());
    m_basic_func_map.clear();
}

QVariant AbstractNode::property_value(PropertyType type) const
{
    auto functor = property_functors(type);
    if (functor)
        return functor->m_getter(this);

    return QVariant();
}

void AbstractNode::update_property(PropertyType type, const QVariant &value)
{
    auto editor_interface = get_editor(type);
    if (editor_interface)
    {
        editor_interface(value);
    }

    if (m_graphics_updater)
        m_graphics_updater(this, type, value);
}

void AbstractNode::update_animation(KeyFrameTime t)
{
    bool show = (t >= m_start_time) and (t <= m_end_time);
    enable(show);
    for( auto info : m_animation_info)
    {
        if (show)
        {
            if (info && info->is_animating)
            {
                int index = -1;
                for (int i = 0; i < info->keyframes.size(); ++i)
                {
                    if (t <= info->keyframes[i]->time())
                    {
                        index = i;
                        break;
                    }
                }

                if (index == -1 && info->keyframes.size())
                    index = info->keyframes.size() - 1;

                if (-1 != index)
                {
                    info->phrases[index]->update(this, t);
                }
            }
        }
    }
}
const QPointF& AbstractNode::position() const
{
    return m_pos;
}

void AbstractNode::set_position(const QPointF &position)
{
    if (m_pos != position)
    {
        m_pos = position;
        update_property(PropertyType::Position,
                        position);
    }
}

const QPointF& AbstractNode::anchor_position() const
{
    return m_anchor_pos;
}

void AbstractNode::set_anchor_position(const QPointF &position)
{
    if (m_anchor_pos != position)
    {
        m_anchor_pos = position;
        update_property(PropertyType::AnchorPosition,
                        position);
    }
}

const QPointF& AbstractNode::get_scale() const
{
    return m_scale;
}

void AbstractNode::set_scale(const Scale &scale)
{
    if (m_scale != scale)
    {
        m_scale = scale;
        update_property(PropertyType::Scale,
                        scale);
    }
}

const qreal& AbstractNode::get_opacity() const
{
    return m_opacity;
}

void AbstractNode::set_opacity(const qreal &opacity)
{
    if (m_opacity != opacity)
    {
        m_opacity = opacity;
        update_property(PropertyType::Opacity,
                        opacity);
    }
}

const qreal& AbstractNode::get_rotation() const
{
    return m_rotation;
}

void AbstractNode::set_rotation(const qreal &rotation)
{
    if (m_rotation != rotation)
    {
        m_rotation = rotation;
        update_property(PropertyType::Rotation,
                        rotation);
    }
}

bool AbstractNode::is_visible() const
{
    return m_is_visible;
}

void AbstractNode::set_visible(bool visible)
{
    if (m_is_visible != visible)
    {
        m_is_visible = visible;
        update_property(PropertyType::Visible,
                        visible);
    }
}

void AbstractNode::enable(bool enable)
{
    m_enabled = enable;
}
