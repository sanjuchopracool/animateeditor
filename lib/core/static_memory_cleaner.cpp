#include "static_memory_cleaner.h"
#include "abstract_node.h"
#include "property_groups_tree.h"

StaticMemoryCleaner::~StaticMemoryCleaner()
{
    clean_static_memory();
}

void StaticMemoryCleaner::clean_static_memory()
{
    AbstractNode::clean_static_memory();
    PropertyGroupsTree::clean_static_memory();
}
