#include "inode.h"

INode::~INode()
{
    for (auto item : m_children)
    {
        delete item;
    }
}

bool INode::add_child(INode *child)
{
    if ((child != m_parent)  and (not m_children.contains(child)))
    {
        if (child->parent())
            child->parent()->remove_child(child);

        m_children.append(child);
        child->set_parent(this);
        return true;
    }

    return false;
}

bool INode::remove_child(INode *child)
{
    if (m_children.removeOne(child))
    {
        child->set_parent(nullptr);
        return true;
    }

    return false;
}

void INode::set_parent(INode *parent)
{
    if (m_parent != parent)
    {
        m_parent = parent;
    }
}
