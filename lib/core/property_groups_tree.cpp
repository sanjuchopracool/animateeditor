#include "property_groups_tree.h"

#include <property.h>
#include <memory>

namespace
{
std::unique_ptr<Property> s_tranform_group;
std::unique_ptr<Property> s_rectangle_group;
}

Property *PropertyGroupsTree::tranform_group()
{
    if (not s_tranform_group)
    {
        s_tranform_group= std::make_unique<Property>(PropertyType::TransformGroup);
        s_tranform_group->add_sub_property(new Property(PropertyType::Position));
        s_tranform_group->add_sub_property(new Property(PropertyType::AnchorPosition));
        s_tranform_group->add_sub_property(new Property(PropertyType::Scale));
        s_tranform_group->add_sub_property(new Property(PropertyType::Opacity));
        s_tranform_group->add_sub_property(new Property(PropertyType::Rotation));
        s_tranform_group->add_sub_property(new Property(PropertyType::Visible));
    }

    return  s_tranform_group.get();
}

Property *PropertyGroupsTree::rectangle_group()
{
    if (not s_rectangle_group)
    {
        s_rectangle_group= std::make_unique<Property>(PropertyType::RectangleGroup);
        s_rectangle_group->add_sub_property(new Property(PropertyType::RectSize));
    }

    return  s_rectangle_group.get();
}

void PropertyGroupsTree::clean_static_memory()
{
    s_tranform_group.reset();
    s_rectangle_group.reset();
}
