#ifndef ANIMATION_PHRASE_H
#define ANIMATION_PHRASE_H

#include <memory>
#include <keyframe.h>

class INode;
class QVariant;
class Interpolate;

struct AnimationPhraseData;
class AnimationPhrase
{
public:
    AnimationPhrase(KeyFrame *start = nullptr, KeyFrame *end = nullptr);
    virtual ~AnimationPhrase();

    void set_lerp(std::unique_ptr<Interpolate> lerp);

    void update(INode* node, KeyFrameTime t);
    void update_lerp();

private:

    void set_start_frame(KeyFrame * start);
    KeyFrame* start_frame() const;

    void set_end_frame(KeyFrame *end);
    KeyFrame* end_frame() const;

    void set_start_value(const QVariant& value);
    const QVariant& start_value() const;

    friend class IAnimationSupport;
private:
    std::unique_ptr<AnimationPhraseData> d;
};

#include <QDebug>
//inline QDebug operator<<(QDebug stream, const AnimationPhrase *phrase)
//{
//    stream << "\n>>>\n( AnimationPhrase :";
//    if (phrase->start_frame())
//    {
//        stream << "\nstart frame: " <<  phrase->start_frame();
//    }
//    else
//    {
//        stream << "\nstart value : " << phrase->start_value();
//    }

//    stream << "\nend frame : " << phrase->end_frame();
//    stream << "\n)\n<<<";
//    return stream;
//}

#endif // ANIMATION_PHRASE_H
