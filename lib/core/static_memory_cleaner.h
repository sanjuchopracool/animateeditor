#ifndef STATIC_MEMORY_CLEANER_H
#define STATIC_MEMORY_CLEANER_H


class StaticMemoryCleaner
{

public:
    ~StaticMemoryCleaner();

private:
    static void clean_static_memory();
};

#endif // STATIC_MEMORY_CLEANER_H
