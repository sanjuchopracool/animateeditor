#ifndef EASING_TYPES_H
#define EASING_TYPES_H

enum KeyFrameEaseType
{
    None    = 0x0,
    In      = 0x1,
    Out     = 0x2,
    InOut   = In | Out
};

#endif // EASING_TYPES_H
