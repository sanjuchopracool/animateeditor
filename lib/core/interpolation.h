#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#include "property_functor_types.h"

#include <QVariant>
#include <QPointF>
#include <memory>

class INode;

class Interpolate
{
public:
    virtual ~Interpolate() {}
    virtual void update(INode* node, double t) = 0;
    virtual void set_start(const QVariant& start) = 0;
    virtual void set_end(const QVariant& end) = 0;
    virtual std::unique_ptr<Interpolate> clone() const = 0;
    virtual std::unique_ptr<Interpolate> hold() const = 0;
};

template <typename Derived, typename PropertyType>
class Hold : public Interpolate
{
public:
    Hold(setter_func_type<Derived, PropertyType> func)
        : m_setter(func) {}

    void update(INode* node, double) override
    {
        if(m_setter)
        {
            (static_cast<Derived*>(node)->*m_setter)(a);
        }
    }

    void set_start(const QVariant& start) override
    {
        a = start.value<PropertyType>();
    }

    void set_end(const QVariant&) override
    {
    }

    std::unique_ptr<Interpolate> clone() const override
    {
         return  std::make_unique<Hold>(*this);
    }

    std::unique_ptr<Interpolate> hold() const override
    {
        return  std::make_unique<Hold>(*this);
    }

private:
    PropertyType a{};
    setter_func_type<Derived, PropertyType> m_setter = nullptr;
};

template <typename Derived, typename PropertyType>
class PropertyInterpolation : public Interpolate
{
public:
    PropertyInterpolation(setter_func_type<Derived, PropertyType> func)
        : m_setter(func) {}

    void update(INode* node, double t) override
    {
        if(m_setter)
        {
            (static_cast<Derived*>(node)->*m_setter)(a + (b - a) * t);
        }
    }

    void set_start(const QVariant& start) override
    {
        a = start.value<PropertyType>();
    }

    void set_end(const QVariant& end) override
    {
        b = end.value<PropertyType>();
    }

    std::unique_ptr<Interpolate> clone() const override
    {
        return  std::make_unique<PropertyInterpolation>(*this);
    }

    std::unique_ptr<Interpolate> hold() const override
    {
        auto result = std::make_unique<Hold<Derived, PropertyType>>(m_setter);
        result->set_start(a);
        return result;
    }

private:
    PropertyType a{};
    PropertyType b{};
    setter_func_type<Derived, PropertyType> m_setter = nullptr;
};

#endif // INTERPOLATION_H
