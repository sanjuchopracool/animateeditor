#ifndef INODE_H
#define INODE_H

#include "property_interface.h"
#include "iserialize.h"
#include "property_functor.h"
#include "ianimation_support.h"
#include "node_types.h"


#include <qglobal.h>
#include <QPointF>

using Scale = QPointF;

class QtVariantPropertyManager;

class INode : virtual public IPropertyInterface,
        public ISerialize,
        public IAnimationSupport
{
public:
    virtual ~INode();

    virtual const QPointF& position() const = 0;
    virtual void set_position(const QPointF &position) = 0;

    virtual const QPointF& anchor_position() const = 0;
    virtual void set_anchor_position(const QPointF &position) = 0;

    virtual const Scale& get_scale() const = 0;
    virtual void set_scale(const Scale &scale) = 0;

    virtual const qreal& get_opacity() const = 0;
    virtual void set_opacity(const qreal &opacity) = 0;

    virtual const qreal& get_rotation() const = 0;
    virtual void set_rotation(const qreal &rotation) = 0;

    virtual bool is_visible() const = 0;
    virtual void set_visible(bool visible) = 0;

    inline QString name() const { return m_name; }
    inline void set_name(const QString& name) { m_name = name; }

    inline int id() const { return m_id; }
    inline void set_id(int id) { m_id = id; }

    virtual bool add_child(INode* child);
    virtual bool remove_child(INode* child);

    const QList<INode*>& children() const {
        return m_children;
    }


    NodeType node_type() const { return m_node_type; }
    void set_node_type(NodeType type) { m_node_type = type; }
    INode* parent() const { return m_parent; }
private:
    void set_parent(INode* parent);

protected:
    QString m_name;
    int m_id = -1;
    INode* m_parent = nullptr;
    QList<INode*> m_children;
    NodeType m_node_type;
};

Q_DECLARE_METATYPE(INode*)

#endif // INODE_H
