#ifndef ABSTRACT_NODE_H
#define ABSTRACT_NODE_H

#include "inode.h"
#include "property.h"

class AbstractNode: public INode
{
public:

    AbstractNode();
    ~AbstractNode() override;

    bool save(QDataStream &out) const override;
    bool load(QDataStream &in) override;

    QList<const Property *> &properties() override;
    QVariant property_value(PropertyType type) const override;
    void update_animation(KeyFrameTime t) override;


    const QPointF& position() const override;
    void set_position(const QPointF &position) override;

    const QPointF& anchor_position() const override;
    void set_anchor_position(const QPointF &position) override;

    const Scale& get_scale() const override;
    void set_scale(const Scale &scale) override;

    const qreal& get_opacity() const override;
    void set_opacity(const qreal &opacity) override;

    const qreal& get_rotation() const override;
    void set_rotation(const qreal &rotation) override;

    bool is_visible() const override;
    void set_visible(bool visible) override;

    virtual void enable(bool enable);

protected:
    void update_property(PropertyType type,
                         const QVariant& value);
    void register_property_functors(PropertyType type,
                                    PropertyFunctor *interface) override;
    PropertyFunctor *property_functors(PropertyType type) const override;

    std::unique_ptr<Interpolate> lerp_for_type(PropertyType type) override;

    void set_graphics_updater(CustomGraphicsUpdater updater = nullptr);

private:
    static void clean_static_memory();

protected:
    QPointF m_pos = {0, 0};
    QPointF m_anchor_pos = {0, 0};
    Scale m_scale = {1, 1};
    qreal m_opacity = 1;
    qreal m_rotation = 0;
    bool m_is_visible = true;
    bool m_enabled = true;

    CustomGraphicsUpdater m_graphics_updater = nullptr;
    friend class StaticMemoryCleaner;
};

#endif // ABSTRACT_NODE_H
