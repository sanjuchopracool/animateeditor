#ifndef PROPERTY_GROUPS_TREE_H
#define PROPERTY_GROUPS_TREE_H

class Property;
class PropertyGroupsTree
{
public:
    static Property *tranform_group();
    static Property *rectangle_group();

private:
    void static clean_static_memory();
    friend class StaticMemoryCleaner;
};

#endif // PROPERTY_GROUPS_TREE_H
