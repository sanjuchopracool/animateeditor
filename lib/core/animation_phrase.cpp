#include "animation_phrase.h"
#include "interpolation.h"
#include "Easing.h"

#include <QVariant>

struct AnimationPhraseData
{
    KeyFrame * start_frame = nullptr;
    KeyFrame * end_frame = nullptr;
    QVariant start_value;
    Interpolate* lerp = nullptr;
    std::unique_ptr<Interpolate> ramp;
    std::unique_ptr<Interpolate> hold;
    std::function<float( float t )> ease_func = choreograph::EaseInOutCubic();

    ~AnimationPhraseData()
    {
    }
};

AnimationPhrase::AnimationPhrase(KeyFrame *start, KeyFrame *end)
    : d (std::make_unique<AnimationPhraseData>())
{
    set_start_frame(start);
    set_end_frame(end);
}

AnimationPhrase::~AnimationPhrase()
{
    if (d->start_frame)
        d->start_frame->set_right_phrase(nullptr);

    if (d->end_frame)
        d->end_frame->set_left_phrase(nullptr);
}

void AnimationPhrase::set_start_frame(KeyFrame *start)
{
     d->start_frame = start;

     if (start)
         d->start_frame->set_right_phrase(this);

     update_lerp();
}

KeyFrame *AnimationPhrase::start_frame() const
{
    return d->start_frame;
}

void AnimationPhrase::set_end_frame(KeyFrame *end)
{
    d->end_frame = end;

    if (end)
        d->end_frame->set_left_phrase(this);

    update_lerp();
}

KeyFrame *AnimationPhrase::end_frame() const
{
    return d->end_frame;
}

void AnimationPhrase::set_start_value(const QVariant &value)
{
    d->start_value = value;
    update_lerp();
}

const QVariant &AnimationPhrase::start_value() const
{
    return d->start_value;
}

void AnimationPhrase::set_lerp(std::unique_ptr<Interpolate> lerp)
{
    d->ramp = std::move(lerp);
    update_lerp();
}

void AnimationPhrase::update(INode* node, KeyFrameTime t)
{
    if (d->lerp == d->hold.get())
    {
        d->lerp->update(node, 1.0);
    }
    else
    {
        double  start_time = d->start_frame->time();
        double end_time = d->end_frame->time();
        double dt = t - start_time;

        int duration = end_time - start_time;

        // do no extrapolate beyond range, use end frame value
        if (dt > duration) dt = duration;

        d->lerp->update(node, d->ease_func(dt/duration));
    }
}

void AnimationPhrase::update_lerp()
{
    if (d->ramp)
    {
        if (d->start_frame)
        {
            d->lerp = d->ramp.get();
            d->lerp->set_start(d->start_frame->value());
            if (d->end_frame)
                d->lerp->set_end(d->end_frame->value());
        }
        else
        {
            if (not d->hold)
                d->hold = d->ramp->hold();

            d->lerp = d->hold.get();
            d->lerp->set_start(d->end_frame->value());
        }
    }
}
