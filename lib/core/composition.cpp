#include "composition.h"

#include <QVersionNumber>
#include "inode_factory.h"
#include "timeline.h"

namespace
{
QVersionNumber s_version(1, 0, 0);

QList<INode*> child_nodes(const INode * node)
{
    QList<INode*> result;
    if(node)
    {
        result.append(const_cast<INode*>(node));
        if (node->node_type() != NodeType::Composition)
        {
            for(const auto& child : node->children())
            {
                result.append(child_nodes(child));
            }
        }
    }

    return result;
}

}

Composition::Composition(INodeFactory *factory, const QString &name)
    : m_factory(factory),
      m_timeline(std::make_unique<TimeLine>())
{
    Q_ASSERT(factory);
    set_node_type(NodeType::Composition);
    set_id(next_id());

    if (name.isEmpty())
        set_name(QString("COMPOSITION_%1").arg(id()));
    else
        set_name(name);
}

bool Composition::save(QDataStream &out) const
{
    out << s_version;
    out << m_last_alloted_id;

    AbstractNode::save(out);
    IAnimationSupport::save_animations(out);
    out << m_children.count();
    for (const auto& child : m_children)
    {
        out << child->node_type();
        save(out, child);
    }

    m_timeline->save(out);
    return true;
}

bool Composition::load(QDataStream &in_stream)
{
    QVersionNumber version;
    in_stream >> version;

    in_stream >> m_last_alloted_id;
    AbstractNode::load(in_stream);
    IAnimationSupport::load_animations(in_stream);
    int children_count = 0;
    in_stream >> children_count;

    NodeType type;
    for (int i = 0; i < children_count; ++i)
    {
        in_stream >> type;
        INode * child = m_factory->create_node(type);
        if (child)
        {
            load(in_stream, child, m_factory);
            add_child(child);
        }
    }

    m_timeline->load(in_stream);
    return true;
}

QList<INode *> Composition::timeline_nodes() const
{
    QList<INode*> result;
    for(const auto& child : children())
    {
        result.append(child_nodes(child));
    }

    return result;
}

TimeLine *Composition::timeline()
{
    return m_timeline.get();
}

int Composition::next_id()
{
    return m_last_alloted_id++;
}

bool Composition::save(QDataStream &out, const INode *node)
{
    if (node)
    {
        node->save(out);
        node->save_animations(out);
        out << node->children().count();
        for (const auto& child : node->children())
        {
            out << child->node_type();
            save(out, child);
        }
    }

    return true;
}

bool Composition::load(QDataStream &in, INode *node, const std::unique_ptr<INodeFactory> &factory)
{
    if (node and factory)
    {
        node->load(in);
        node->load_animations(in);
        int children_count = 0;
        in >> children_count;

        NodeType type;
        for (int i = 0; i < children_count; ++i)
        {
            in >> type;
            INode * child = factory->create_node(type);
            if (child)
            {
                load(in, child, factory);
                node->add_child(child);
            }
        }
    }

    return true;
}
