#ifndef KEYFRAME_H
#define KEYFRAME_H

#include <numeric>
#include <easing_types.h>
#include <QVariant>
#include <memory>

using KeyFrameTime = int;
const static KeyFrameTime KeyFrameMaxTime = std::numeric_limits<int>::max();
const static KeyFrameTime KeyFrameMinTime = -std::numeric_limits<int>::max();

class AnimationPhrase;
struct KeyFrameData;
class KeyFrame
{
public:
    KeyFrame(KeyFrameTime t, const QVariant &value = QVariant(), KeyFrameEaseType type = KeyFrameEaseType::None);
    ~KeyFrame();

    KeyFrameTime time() const;

    void add_easing(KeyFrameEaseType type);
    KeyFrameEaseType ease_type() const;

    void set_easing(KeyFrameEaseType type);

    QVariant value() const;
    void set_value(const QVariant &value);

private:
    void set_left_phrase(AnimationPhrase* phrase);
    void set_right_phrase(AnimationPhrase* phrase);
    void set_time(KeyFrameTime t);
    void update_phrases();

    friend class AnimationPhrase;
    friend class IAnimationSupport;
private:
    std::unique_ptr<KeyFrameData> d;
};

inline bool operator == (const KeyFrame &lhs, const KeyFrame & rhs)
{
    return lhs.time() == rhs.time();
}

inline bool operator > (const KeyFrame &lhs, const KeyFrame & rhs)
{
    return lhs.time() > rhs.time();
}


#include <QDebug>
inline QDebug operator<<(QDebug stream, const KeyFrame *frame)
{
    stream << "( KeyFrame : at time ";
    stream << frame->time() << " with value ";
    stream << frame->value() << ")";
    return stream;
}

#endif // KEYFRAME_H
