#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "abstract_node.h"
#include <QColor>

using Size = QPointF;
class Rectangle : public AbstractNode
{
public:
    Rectangle();
    ~Rectangle();

    QList<const Property *> &properties() override;

    bool save(QDataStream &out) const override;
    bool load(QDataStream &in) override;

    virtual void set_size(const Size& size);
    virtual const Size& get_size() const;

    virtual void set_fill_color(const QColor& color);
    virtual QColor fill_color() const;

protected:
    Size m_size{100.0, 100};
    QColor m_fill_color;
};

#endif // RECTANGLE_H
