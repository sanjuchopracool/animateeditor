#include "rectangle.h"

#include <QVersionNumber>
#include <property_groups_tree.h>

namespace
{
QVersionNumber s_version(1, 0, 0);
QList<const Property*> m_basic_properties;
}

Rectangle::Rectangle()
{
    set_node_type(NodeType::Rectangle);
}

Rectangle::~Rectangle()
{

}

QList<const Property *> &Rectangle::properties()
{
    if (m_basic_properties.isEmpty())
    {
        m_basic_properties.append(AbstractNode::properties());
        m_basic_properties.append(PropertyGroupsTree::rectangle_group());

        PropertyFunctor *rect_size_functor = new PointFFunctor<Rectangle>(&Rectangle::set_size,
                                                                          &Rectangle::get_size);
        register_property_functors(PropertyType::RectSize, rect_size_functor);
    }

    return  m_basic_properties;
}

bool Rectangle::save(QDataStream &out) const
{
    AbstractNode::save(out);

    out << s_version;
    out << m_size;
    out << m_fill_color;
    return true;
}

bool Rectangle::load(QDataStream &in)
{
    AbstractNode::load(in);

    QVersionNumber version;
    in >> version;

    Size size;
    in >> size;
    set_size(size);

    QColor fill_color;
    in >> fill_color;
    set_fill_color(fill_color);
    return true;
}

void Rectangle::set_size(const Size &size)
{
    if (m_size != size )
    {
        m_size = size;
        update_property(PropertyType::RectSize,
                        size);
    }
}

const Size &Rectangle::get_size() const
{
    return m_size;
}

void Rectangle::set_fill_color(const QColor &color)
{
    m_fill_color = color;
}

QColor Rectangle::fill_color() const
{
    return m_fill_color;
}
