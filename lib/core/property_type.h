#ifndef PROPERTY_TYPE_H
#define PROPERTY_TYPE_H

#include <QDataStream>

enum class PropertyType
{
    None = 0,
    TransformGroup,
    RectangleGroup,
    LastGroup,
    MaxGroup = 0xFF,

    Position,
    AnchorPosition,
    Scale,
    Opacity,
    Rotation,
    Visible,

    RectSize,
    RectFillColor
};

inline bool is_property_group(PropertyType type)
{
    return (type <= PropertyType::LastGroup && type > PropertyType::None );
}

inline bool operator < (PropertyType lhs, PropertyType rhs)
{
    return (static_cast<int>(lhs) < static_cast<int>(rhs));
}

inline bool operator == (PropertyType lhs, PropertyType rhs)
{
    return (static_cast<int>(lhs) == static_cast<int>(rhs));
}

inline unsigned qHash(const PropertyType &t)
{
    return static_cast<int>(t);
}

inline QDataStream & operator<<(QDataStream &out, PropertyType type) {
    out << static_cast<unsigned>(type);
    return out;
}

inline QDataStream & operator>>(QDataStream &in, PropertyType& node_type) {
    unsigned type;
    in >> type;

    node_type = static_cast<PropertyType>(type);
    return in;
}
#endif // PROPERTY_TYPE_H
