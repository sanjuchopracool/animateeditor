#ifndef COMPOSITION_H
#define COMPOSITION_H

#include "abstract_node.h"
#include <memory>

class INodeFactory;
class TimeLine;
class Composition : public AbstractNode
{
public:
    Composition(INodeFactory *factory, const QString& name = QString());
    ~Composition() = default;

    bool save(QDataStream &out) const override;
    bool load(QDataStream &in) override;

    QList<INode *> timeline_nodes() const;

    TimeLine* timeline();
private:
    int next_id();

    static bool save(QDataStream &out, const INode* node);
    static bool load(QDataStream &in, INode* node, const std::unique_ptr<INodeFactory>& factory);
protected:
    int m_last_alloted_id = 0;
     std::unique_ptr<INodeFactory> m_factory;
     std::unique_ptr<TimeLine> m_timeline;
};

#endif // COMPOSITION_H
