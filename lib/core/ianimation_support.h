#ifndef IANIMATION_SUPPORT_H
#define IANIMATION_SUPPORT_H

#include <keyframe.h>
#include "property_type.h"
#include "animation_phrase.h"
#include "interpolation.h"
#include <QVariant>
#include <memory>

struct PropertyAnimationInfo
{
    bool is_animating = false;
    QList<KeyFrame*> keyframes;
    QList<AnimationPhrase*> phrases;

    ~PropertyAnimationInfo()
    {
        qDeleteAll(phrases);
        qDeleteAll(keyframes);
    }
};

class IAnimationSupport
{
public:
    IAnimationSupport();
    virtual ~IAnimationSupport();

    virtual QVariant property_value(PropertyType type) const = 0;

    virtual std::unique_ptr<Interpolate> lerp_for_type(PropertyType type) = 0;

    bool is_animating(PropertyType type) const;
    void animate_property(PropertyType type, bool animate = true);

    QList<KeyFrame *> keyframes(PropertyType type) const;

    void clear_keyframes(PropertyType type);
    KeyFrame *add_keyframe(PropertyType type, KeyFrameTime t, const QVariant& val);
    void remove_keyframe(PropertyType type, KeyFrame *keyframe);
    KeyFrame * update_keyframe(PropertyType type,
                               KeyFrame *keyframe,
                               KeyFrameTime t,
                               bool force_update = false);

    void update_keyframe_value(PropertyType type, KeyFrameTime t, const QVariant& val);


    void set_start_time(KeyFrameTime t);
    void move_time_range(KeyFrameTime t);
    void set_end_time(KeyFrameTime t);

    KeyFrameTime start_time() const;
    KeyFrameTime end_time() const;

    virtual void update_animation(KeyFrameTime t) = 0;

    bool save_animations(QDataStream &out) const;
    bool load_animations(QDataStream &in);

private:
    void on_frame_added(PropertyType type, KeyFrame *frame);

protected:
    QMap<PropertyType, PropertyAnimationInfo*> m_animation_info;
    KeyFrameTime m_start_time = 0;
    KeyFrameTime m_end_time = KeyFrameMaxTime;

};

#endif // IANIMATION_SUPPORT_H
