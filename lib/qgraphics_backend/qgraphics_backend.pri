
INCLUDEPATH +=$$PWD

HEADERS += \
    $$PWD/graphics_node_factory.h \
    $$PWD/graphics_item_node.h \
    $$PWD/graphics_rectangle.h

SOURCES += \
    $$PWD/graphics_node_factory.cpp \
    $$PWD/graphics_item_node.cpp \
    $$PWD/graphics_rectangle.cpp
