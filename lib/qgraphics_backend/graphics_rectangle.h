#ifndef GRAPHICS_RECTANGLE_H
#define GRAPHICS_RECTANGLE_H

#include "graphics_item_node.h"
#include <rectangle.h>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

class GraphicsRectangle : public GraphicsItemNode<Rectangle>
{
public:
    GraphicsRectangle(QGraphicsItem *parent = nullptr)
        : GraphicsItemNode<Rectangle>(parent)
    {
        set_graphics_updater(GraphicsUpdateTemplate<GraphicsRectangle>(&GraphicsRectangle::update_graphics_item));
    }

    void update_graphics_item(PropertyType type, const QVariant &value)
    {
        bool update_item = true;
        switch (type)
        {
        case PropertyType::RectSize:
            prepareGeometryChange();
            break;
        default:
            update_item = false;
            break;
        }

        if (update_item)
            update();
        else
            GraphicsItemNode<Rectangle>::update_graphics_item(type, value);
    }

    QRectF boundingRect() const override
    {
        double width = Rectangle::m_size.x();
        double height = Rectangle::m_size.y();
        QRectF rect = QRectF(-width/2, -height/2, width, height);
        return rect;
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override
    {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        painter->fillRect(option->rect, Rectangle::m_fill_color);
    }
};

#endif // GRAPHICS_RECTANGLE_H
