#ifndef GRAPHICS_ITEM_NODE_H
#define GRAPHICS_ITEM_NODE_H

#include <rectangle.h>
#include <QGraphicsItem>

template <typename Derived>
class GraphicsItemNode : public QGraphicsItem , public Derived
{
public:
    GraphicsItemNode(QGraphicsItem *parent = nullptr)
        :QGraphicsItem(parent)
    {
    }

    ~GraphicsItemNode() {

    }

    void update_graphics_item(PropertyType type, const QVariant &value)
    {
        bool update_item = true;
        switch (type)
        {
        case PropertyType::Position:
            setPos(value.toPointF());
            break;
        case PropertyType::AnchorPosition:
            resetMatrix();
            break;
        case PropertyType::Scale:
            reset_matrix();
            break;
        case PropertyType::Rotation:
            reset_matrix();
            break;
        case PropertyType::Opacity:
            setOpacity(value.toReal());
            break;
        case PropertyType::Visible:
            setVisible(value.toBool());
            break;
        default:
            update_item = false;
            break;
        }

        if (update_item)
            update();
    }

    bool add_child(INode* child) override
    {
        if (INode::add_child(child))
        {
            static_cast<GraphicsItemNode*>(child)->setParentItem(this);
            return true;
        }

        return false;
    }

    bool remove_child(INode* child) override {
        if (INode::remove_child(child))
        {
            static_cast<GraphicsItemNode*>(child)->setParentItem(nullptr);
            return true;
        }

        return false;
    }

    QRectF boundingRect() const override
    {
        return QRectF();
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override
    {
        Q_UNUSED(painter)
        Q_UNUSED(option)
        Q_UNUSED(widget)
    }

protected:
    void reset_matrix()
    {
        QTransform trans;
        trans.translate(Derived::m_anchor_pos.x(), Derived::m_anchor_pos.y());
        trans.rotate(Derived::m_rotation);
        trans.scale(Derived::m_scale.x(), Derived::m_scale.y());
        trans.translate(-Derived::m_anchor_pos.x(), -Derived::m_anchor_pos.y());
        setTransform(trans);
    }
};

#endif // GRAPHICS_ITEM_NODE_H
