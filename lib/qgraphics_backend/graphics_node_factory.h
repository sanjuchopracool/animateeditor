#ifndef GRAPHICS_NODE_FACTORY_H
#define GRAPHICS_NODE_FACTORY_H

#include "inode_factory.h"

class GraphicsNodeFactory : public INodeFactory
{
public:
    INode *create_node(NodeType type) override;
};

#endif // GRAPHICS_NODE_FACTORY_H
