#include "graphics_node_factory.h"

#include "graphics_item_node.h"
#include "graphics_rectangle.h"

INode *GraphicsNodeFactory::create_node(NodeType type)
{
    INode * result = nullptr;
    switch (type) {
    case NodeType::Abstract:
        result = new GraphicsItemNode<AbstractNode>;
        break;
    case NodeType::Rectangle:
        result = new GraphicsRectangle;
        break;
    default:
        break;
    }

    return result;
}
