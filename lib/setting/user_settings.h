#ifndef USER_SETTINGS_H
#define USER_SETTINGS_H

#include <canvas_color_setting.h>

class UserSettings
{

public:

    static CanvasColorSetting canvas_color_setting();
    static void set_canvas_color_setting(const CanvasColorSetting &setting);

    static void save_timeline_splitter_state(const QByteArray& state);
    static QByteArray timeline_splitter_state();

    static void save_main_splitter_state(const QByteArray& state);
    static QByteArray main_splitter_state();

    static void save_main_window_geometry_state(const QByteArray& state);
    static QByteArray main_window_geometry_state();

    static void save_last_opened_comp_dir(const QString& last_path);
    static QString last_opened_comp_dir();
};

#endif // USER_SETTINGS_H
