#ifndef CONFIGCONSTANTS_H
#define CONFIGCONSTANTS_H

const char ORGANIZATION_NAME[] = "sanjuchopracolor";
const char APP_NAME[] = "animate";
const char APP_VERSION[] = "1.0.0";
const char ORGANISATION_DOMAIN[] = "dalitengineers.com";

#endif // CONFIGCONSTANTS_H
