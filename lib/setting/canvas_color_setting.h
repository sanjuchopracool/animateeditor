#ifndef CANVAS_COLOR_SETTING_H
#define CANVAS_COLOR_SETTING_H

#include <QColor>

struct CanvasColorSetting
{
    QColor bg_color;
    QColor canvas_color;
};

#endif // CANVAS_COLOR_SETTING_H
