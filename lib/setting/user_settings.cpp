#include "user_settings.h"
#include <QSettings>
#include <config_constants.h>

namespace
{
    QSettings user_setting(QSettings::UserScope,
                           ORGANIZATION_NAME,
                           APP_NAME);

    const QString canvas_group("canvas");
    const QString bg_color("bg_color");

    const QString timeline_group("canvas");
    const QString timeline_splitter_state_key("timline_splitter_state");

    const QString main_window_group("main_window");
    const QString main_window_splitter_state_key("main_window_splitter_state");
    const QString main_window_geometry_state_key("main_window_geometry_state");
    const QString last_opened_comp_dir_key("last_opened_comp_dir");
}

CanvasColorSetting UserSettings::canvas_color_setting()
{
    CanvasColorSetting result;
    user_setting.beginGroup(canvas_group);
    result.bg_color = user_setting.value(bg_color, QColor("#454545")).value<QColor>();
    user_setting.endGroup();
    return  result;
}

void UserSettings::set_canvas_color_setting(const CanvasColorSetting &setting)
{
    user_setting.beginGroup(canvas_group);
    user_setting.setValue(bg_color, setting.bg_color);
    user_setting.endGroup();
}

void UserSettings::save_timeline_splitter_state(const QByteArray &state)
{
    user_setting.beginGroup(timeline_group);
    user_setting.setValue(timeline_splitter_state_key, state);
    user_setting.endGroup();
}

QByteArray UserSettings::timeline_splitter_state()
{
    QByteArray result;
    user_setting.beginGroup(timeline_group);
    result = user_setting.value(timeline_splitter_state_key).toByteArray();
    user_setting.endGroup();
    return result;
}

void UserSettings::save_main_splitter_state(const QByteArray &state)
{
    user_setting.beginGroup(main_window_group);
    user_setting.setValue(main_window_splitter_state_key, state);
    user_setting.endGroup();
}

QByteArray UserSettings::main_splitter_state()
{
    QByteArray result;
    user_setting.beginGroup(main_window_group);
    result = user_setting.value(main_window_splitter_state_key).toByteArray();
    user_setting.endGroup();
    return result;
}

void UserSettings::save_main_window_geometry_state(const QByteArray &state)
{
    user_setting.beginGroup(main_window_group);
    user_setting.setValue(main_window_geometry_state_key, state);
    user_setting.endGroup();
}

QByteArray UserSettings::main_window_geometry_state()
{
    QByteArray result;
    user_setting.beginGroup(main_window_group);
    result = user_setting.value(main_window_geometry_state_key).toByteArray();
    user_setting.endGroup();
    return result;
}

void UserSettings::save_last_opened_comp_dir(const QString &last_path)
{
    user_setting.beginGroup(main_window_group);
    user_setting.setValue(last_opened_comp_dir_key, last_path);
    user_setting.endGroup();
}

QString UserSettings::last_opened_comp_dir()
{
    QString result;
    user_setting.beginGroup(main_window_group);
    result = user_setting.value(last_opened_comp_dir_key).toString();
    user_setting.endGroup();
    return result;
}
