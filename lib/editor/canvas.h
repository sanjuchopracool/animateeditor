#ifndef CANVAS_H
#define CANVAS_H

#include <QGraphicsView>
#include <canvas_color_setting.h>

class Canvas : public QGraphicsView
{
    Q_OBJECT
public:
    explicit Canvas(QWidget *parent = nullptr);
    ~Canvas();

protected:
    void wheelEvent(QWheelEvent *) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;

private:
    void reset_matrix();

    void zoom_in(int level = 10);
    void zoom_out(int level = 10);

private:
    int m_zoom_value = 0;
    bool m_is_panning = false;
    QPoint m_last_pan_pos;

    CanvasColorSetting m_color_setting;
};

#endif // CANVAS_H
