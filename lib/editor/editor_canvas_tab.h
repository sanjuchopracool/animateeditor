#ifndef EDITOR_CANVAS_TAB_H
#define EDITOR_CANVAS_TAB_H

#include <QWidget>

class INode;
class Canvas;
class QGraphicsScene;

class EditorCanvasTab : public QWidget
{
    Q_OBJECT
public:
    explicit EditorCanvasTab(INode *node, QWidget *parent = nullptr);
    ~EditorCanvasTab();

    QString name() const;
    INode *node() const { return m_node; }

    void add_node(INode * node);
signals:

private:
    INode *m_node = nullptr;
    Canvas * m_canvas = nullptr;
    QGraphicsScene *m_scene = nullptr;
};

#endif // EDITOR_CANVAS_TAB_H
