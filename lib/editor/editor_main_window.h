#ifndef EDITOR_MAIN_WINDOW_H
#define EDITOR_MAIN_WINDOW_H

#include <QMainWindow>
#include <QMap>

class EditorTimelineArea;
class TimelineWidget;
class EditorCanvasArea;
class EditorCanvasTab;
class QSplitter;
class QAction;
class QFile;
class INode;
class Composition;

struct CompositionWidgets
{
    EditorCanvasTab * canvas_tab = nullptr;
    TimelineWidget * timeline_tab = nullptr;
};

class EditorMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit EditorMainWindow(QWidget *parent = nullptr);
    ~EditorMainWindow() override;

    void update_theme();
    void open_compostion(Composition * new_comp);
signals:

private:
    void create_actions();
    void create_menus();

private slots:
    void open();
    void close_current_Compositon();
    void save_current_Compositon();
    void toggle_timeline_state();

private:
    void open_composition_from_file(QFile& opened_file);
    void close_composition(Composition *node);

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    EditorTimelineArea * m_timeline_area = nullptr;
    EditorCanvasArea * m_canvas_area = nullptr;
    QSplitter * m_main_splitter = nullptr;

    // menus
    QMenu * m_file_menu = nullptr;
    QMenu * m_timeline_menu = nullptr;

    // actions
    QAction *m_open_action = nullptr;
    QAction *m_close_action = nullptr;
    QAction *m_save_action = nullptr;

    QAction *m_timeline_play_pause_action = nullptr;

    //
    QMap<Composition*, CompositionWidgets> m_node_widgets;
};

#endif // EDITOR_MAIN_WINDOW_H
