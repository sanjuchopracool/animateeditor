
INCLUDEPATH +=$$PWD

HEADERS += \
    $$PWD/canvas.h \
    $$PWD/editor_main_window.h \
    $$PWD/editor_timeline_area.h \
    $$PWD/editor_canvas_area.h \
    $$PWD/editor_canvas_tab.h \
    $$PWD/editor_theme.h

SOURCES += \
    $$PWD/canvas.cpp \
    $$PWD/editor_main_window.cpp \
    $$PWD/editor_timeline_area.cpp \
    $$PWD/editor_canvas_area.cpp \
    $$PWD/editor_canvas_tab.cpp \
    $$PWD/editor_theme.cpp
