#ifndef EDITOR_TIMELINE_AREA_H
#define EDITOR_TIMELINE_AREA_H

#include <QTabWidget>

class TimelineWidget;
class Composition;

class EditorTimelineArea : public QTabWidget
{
    Q_OBJECT
public:
    EditorTimelineArea(QWidget * parent = nullptr);

    void add_timeline_tab(TimelineWidget *tab);
    void update_theme();
    Composition *current_composition() const;
signals:
    void canvasClickedForNode(Composition*);
};

#endif // EDITOR_TIMELINE_AREA_H
