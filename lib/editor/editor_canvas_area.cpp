#include "editor_canvas_area.h"

#include "editor_canvas_tab.h"
#include "editor_theme.h"

#include <QTabBar>

EditorCanvasArea::EditorCanvasArea(QWidget *parent)
    : QTabWidget(parent)
{
    connect( this, &QTabWidget::currentChanged, this, [=](int)
    {
        EditorCanvasTab * canvasTab = static_cast<EditorCanvasTab*>(currentWidget());
        if (canvasTab)
        {
            emit canvasClickedForNode(canvasTab->node());
        }
    });
}

void EditorCanvasArea::add_canvas_tab(EditorCanvasTab *tab)
{
    if (tab)
    {
        addTab(tab, tab->name());
    }
}

void EditorCanvasArea::update_theme()
{
    setStyleSheet(EditorTheme::tab_area_style_sheet());
}
