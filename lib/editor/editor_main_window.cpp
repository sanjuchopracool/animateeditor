#include "editor_main_window.h"

#include "editor_timeline_area.h"
#include "editor_canvas_area.h"
#include "editor_canvas_tab.h"
#include "timeline_widget.h"
#include "editor_theme.h"

#include <composition.h>
#include <graphics_node_factory.h>
#include <user_settings.h>

#include <QSplitter>
#include <QVBoxLayout>

#include <QAction>
#include <QFileDialog>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <timeline.h>

EditorMainWindow::EditorMainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setMinimumSize(800, 500);
    m_timeline_area = new EditorTimelineArea;
    m_canvas_area = new EditorCanvasArea;

    m_main_splitter = new QSplitter(Qt::Vertical);
    m_main_splitter->addWidget(m_canvas_area);
    m_main_splitter->addWidget(m_timeline_area);
    m_main_splitter->setStretchFactor(0,3);
    m_main_splitter->setStretchFactor(1,1);
    m_main_splitter->restoreState(UserSettings::main_splitter_state());
    setCentralWidget(m_main_splitter);

    create_actions();
    create_menus();
    update_theme();
    restoreGeometry(UserSettings::main_window_geometry_state());

    connect(m_canvas_area, &EditorCanvasArea::canvasClickedForNode,
            this, [=](INode * node)
    {
        for (const auto& comp : m_node_widgets.keys())
        {
            if (comp == node)
            {
                m_timeline_area->setCurrentWidget(m_node_widgets.value(comp).timeline_tab);
            }
        }
    });

    connect(m_timeline_area, &EditorTimelineArea::canvasClickedForNode,
            this, [=](Composition * node)
    {
        if (m_node_widgets.contains(node))
        {
            m_canvas_area->setCurrentWidget(m_node_widgets.value(node).canvas_tab);
        }
    });
}

EditorMainWindow::~EditorMainWindow()
{
    for(auto node : m_node_widgets.keys())
        close_composition(node);
}

void EditorMainWindow::update_theme()
{
    m_main_splitter->setContentsMargins(0, 0, 0, 0);
    QString style_sheet(R"(
                        background-color: %1;
                        color:white;
                        )");

    m_main_splitter->setHandleWidth(1);
    m_main_splitter->setStyleSheet(QString(R"(
                                           QSplitter::handle
                                           {
                                           background-color: %1;
                                           }
                                           )").arg(EditorTheme::background_color().name()));

    setStyleSheet(style_sheet.arg(EditorTheme::background_color().name()));
    m_canvas_area->update_theme();
    m_timeline_area->update_theme();
}

void EditorMainWindow::open_compostion(Composition *comp)
{
    CompositionWidgets widgets;
    widgets.canvas_tab = new EditorCanvasTab(comp);
    widgets.timeline_tab = new TimelineWidget(comp);

    m_canvas_area->add_canvas_tab(widgets.canvas_tab);
    m_timeline_area->add_timeline_tab(widgets.timeline_tab);
    m_node_widgets.insert(comp, widgets);

    for( const auto& item : comp->children())
    {
        widgets.canvas_tab->add_node(item);
    }

    for( const auto& item : comp->timeline_nodes())
    {
        widgets.timeline_tab->add_node(item);
    }

    m_canvas_area->setCurrentWidget(widgets.canvas_tab);
    m_timeline_area->setCurrentWidget(widgets.timeline_tab);
}

void EditorMainWindow::create_actions()
{
    m_open_action = new QAction(tr("&Open..."), this);
    m_open_action->setShortcuts(QKeySequence::Open);
    m_open_action->setStatusTip(tr("Open an existing file"));
    connect(m_open_action, &QAction::triggered, this, &EditorMainWindow::open);

    m_close_action = new QAction(tr("&Close"), this);
    m_close_action->setShortcut(QKeySequence::Close);
    m_close_action->setStatusTip(tr("Close current composition"));
    connect(m_close_action, &QAction::triggered, this, &EditorMainWindow::close_current_Compositon);

    m_save_action = new QAction(tr("&Save"), this);
    m_save_action->setShortcut(QKeySequence::Save);
    m_save_action->setStatusTip(tr("Save current composition"));
    connect(m_save_action, &QAction::triggered, this, &EditorMainWindow::save_current_Compositon);

    m_timeline_play_pause_action = new QAction(tr("&Play/Pause"));
    m_timeline_play_pause_action->setShortcut(QKeySequence(Qt::Key_Space));
    m_timeline_play_pause_action->setStatusTip(tr("Play/Pause current composition"));
    connect(m_timeline_play_pause_action, &QAction::triggered, this, &EditorMainWindow::toggle_timeline_state);
}

void EditorMainWindow::create_menus()
{
    m_file_menu = menuBar()->addMenu(tr("&File"));
    m_file_menu->addAction(m_open_action);
    m_file_menu->addAction(m_close_action);
    m_file_menu->addAction(m_save_action);

    m_timeline_menu = menuBar()->addMenu(tr("&Timeline"));
    m_timeline_menu->addAction(m_timeline_play_pause_action);
}

void EditorMainWindow::open()
{
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open Composition"),
                                                     UserSettings::last_opened_comp_dir());
    if (file_name.isEmpty())
        return;

    QFile file(file_name);
    if (!file.open(QFile::ReadOnly)) {
        QString msg = tr("Failed to open %1\n%2")
                        .arg(QDir::toNativeSeparators(file_name), file.errorString());

        QMessageBox::warning(this, tr("Errors"), msg);
        return;
    }

    UserSettings::save_last_opened_comp_dir(QFileInfo(file).absoluteDir().absolutePath());
    open_composition_from_file(file);
}

void EditorMainWindow::close_current_Compositon()
{
    auto comp = m_timeline_area->current_composition();
    if (comp)
        close_composition(comp);
}

void EditorMainWindow::save_current_Compositon()
{
    auto comp = m_timeline_area->current_composition();
    if (comp)
    {
        QString file_name = QFileDialog::getSaveFileName(this, tr("Save Composition"),
                                                         UserSettings::last_opened_comp_dir());

        QFile file(file_name);
        if( !file.open( QIODevice::WriteOnly ) )
        {
            qWarning() << file.errorString();
        }

        QDataStream stream( &file );
        stream.setVersion( QDataStream::Qt_5_12 );
        comp->save(stream);
        file.close();
    }
}

void EditorMainWindow::toggle_timeline_state()
{

    auto comp = m_timeline_area->current_composition();
    if (comp)
    {
        auto timeline = static_cast<Composition*>(comp)->timeline();
        bool is_running =  (timeline->state() == QTimeLine::Running);
        if (is_running)
            timeline->stop();
        else
            timeline->resume();
    }
}

void EditorMainWindow::open_composition_from_file(QFile& opened_file)
{
    if (opened_file.isOpen())
    {
        QDataStream comp_stream(&opened_file);
        comp_stream.setVersion( QDataStream::Qt_5_12 );

        Composition * new_comp = new Composition(new GraphicsNodeFactory);
        new_comp->load(comp_stream);
        new_comp->set_name(QFileInfo(opened_file).baseName());
        open_compostion(new_comp);
    }
}

void EditorMainWindow::close_composition(Composition *node)
{
    if (m_node_widgets.contains(node))
    {
        auto widgets = m_node_widgets.value(node);
        if (widgets.canvas_tab)
        {
            m_canvas_area->removeTab(m_canvas_area->indexOf(widgets.canvas_tab));
            delete widgets.canvas_tab;
        }

        if (widgets.timeline_tab)
        {
            m_timeline_area->removeTab(m_timeline_area->indexOf(widgets.timeline_tab));
            delete widgets.timeline_tab;
        }

        m_node_widgets.remove(node);
        delete node;
    }
}

void EditorMainWindow::closeEvent(QCloseEvent *event)
{
    UserSettings::save_main_splitter_state(m_main_splitter->saveState());
    UserSettings::save_main_window_geometry_state(saveGeometry());
    QMainWindow::closeEvent(event);
}
