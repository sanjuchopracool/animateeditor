#include "editor_timeline_area.h"

#include "timeline_widget.h"
#include "editor_theme.h"

EditorTimelineArea::EditorTimelineArea(QWidget *parent)
    : QTabWidget(parent)
{
    connect( this, &QTabWidget::currentChanged, this, [=](int)
    {
        TimelineWidget * timelineTab = static_cast<TimelineWidget*>(currentWidget());
        if (timelineTab)
        {
            emit canvasClickedForNode(timelineTab->composition());
        }
    });
}

void EditorTimelineArea::add_timeline_tab(TimelineWidget *tab)
{
    if (tab)
    {
        addTab(tab, tab->name());
    }
}

void EditorTimelineArea::update_theme()
{
    setStyleSheet(EditorTheme::tab_area_style_sheet());
}

Composition *EditorTimelineArea::current_composition() const
{
    TimelineWidget * timelineTab = static_cast<TimelineWidget*>(currentWidget());
    if (timelineTab)
    {
        return timelineTab->composition();
    }

    return nullptr;
}
