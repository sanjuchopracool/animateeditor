#include "editor_theme.h"

namespace
{

QColor m_background_color("#454545");
QColor m_foreground_color(Qt::white);
QColor m_background_line_color(Qt::black);
}

QColor EditorTheme::background_color()
{
    return m_background_color;
}

QColor EditorTheme::foreground_color()
{
    return m_foreground_color;
}

QColor EditorTheme::background_line_color()
{
    return m_background_line_color;
}

QString EditorTheme::tab_area_style_sheet()
{
    QString style_sheet(R"(
                        QTabWidget::pane {
                        border: 0px;
                        }

                        QTabWidget::tab-bar {
                        left: 0px;
                        }

                        QTabBar::tab {
                        background: %1;
                        color:%2;
                        padding: 5px;
                        }

                        QTabBar::tab:selected {
                        border: 1px solid %3;
                        border-bottom-width: 0px;
                        }

                        )");

    return (style_sheet.arg(EditorTheme::background_color().name())
            .arg(EditorTheme::foreground_color().name())
            .arg(EditorTheme::background_line_color().name()));
}
