#include "canvas.h"

#include <user_settings.h>

#include <QApplication>
#include <QScrollBar>
#include <QtMath>
#include <QWheelEvent>

#include <QDebug>

namespace
{
    const int max_zoom_value = 500;
    const int min_zoom_value = 0;
    const int default_zoom_value = 250;
}

Canvas::Canvas(QWidget *parent)
    : QGraphicsView(parent),
      m_zoom_value(default_zoom_value),
      m_color_setting(UserSettings::canvas_color_setting())
{
    setRenderHint(QPainter::Antialiasing, true);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    setBackgroundBrush(m_color_setting.bg_color);
    setCacheMode(QGraphicsView::CacheBackground);

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

Canvas::~Canvas()
{

}

void Canvas::wheelEvent(QWheelEvent *event)
{
    if (event->modifiers() & Qt::ControlModifier)
    {
        const ViewportAnchor anchor = transformationAnchor();
        setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        if(event->delta() > 0)
            zoom_in();
        else
            zoom_out();

        setTransformationAnchor(anchor);
        event->accept();
    }
    else
    {
        QGraphicsView::wheelEvent(event);
    }
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    if (    Qt::MiddleButton == event->button()
         && Qt::NoModifier == event->modifiers())
    {
        m_is_panning = true;
        m_last_pan_pos = event->pos();
        event->accept();
        QApplication::setOverrideCursor(Qt::ClosedHandCursor);
    }
    else
    {
        QGraphicsView::mousePressEvent(event);
    }
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    if (    Qt::MiddleButton == event->button()
         && Qt::NoModifier == event->modifiers())
    {
        m_is_panning = false;
        QApplication::restoreOverrideCursor();
        event->accept();
    }
    else
    {
        QGraphicsView::mouseReleaseEvent(event);
    }
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    if (   m_is_panning
        && (event->buttons() &= Qt::MiddleButton)
        && (Qt::NoModifier == event->modifiers()))
    {
        QPoint new_pos = event->pos();
        QPointF movement = -((new_pos) - (m_last_pan_pos));
        m_last_pan_pos = event->pos();
        horizontalScrollBar()->setValue(horizontalScrollBar()->value() + movement.x());
        verticalScrollBar()->setValue(verticalScrollBar()->value() + movement.y());
        event->accept();
    }
    else
    {
        QGraphicsView::mouseMoveEvent(event);
    }
}

void Canvas::reset_matrix()
{
    qreal scale = qPow(qreal(2), (m_zoom_value - 250) / qreal(50));
    QMatrix matrix;
    matrix.scale(scale, scale);
    setMatrix(matrix);
}

void Canvas::zoom_in(int level)
{
    m_zoom_value += level;
    if (m_zoom_value > max_zoom_value)
        m_zoom_value = max_zoom_value;

    reset_matrix();
}

void Canvas::zoom_out(int level)
{
    m_zoom_value -= level;
    if (m_zoom_value < min_zoom_value)
        m_zoom_value = min_zoom_value;

    reset_matrix();
}
