#ifndef EDITOR_THEME_H
#define EDITOR_THEME_H

#include <QColor>

class EditorTheme
{
public:
    static QColor background_color();
    static QColor foreground_color();
    static QColor background_line_color();
    static QString tab_area_style_sheet();
};

#endif // EDITOR_THEME_H
