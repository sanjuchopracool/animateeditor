#ifndef EDITOR_CANVAS_AREA_H
#define EDITOR_CANVAS_AREA_H

#include <QTabWidget>

class EditorCanvasTab;
class INode;
class EditorCanvasArea : public QTabWidget
{
    Q_OBJECT
public:
    explicit EditorCanvasArea(QWidget *parent = nullptr);

    void add_canvas_tab(EditorCanvasTab * tab);

    void update_theme();
signals:
    void canvasClickedForNode(INode*);

public slots:
};

#endif // EDITOR_CANVAS_AREA_H
