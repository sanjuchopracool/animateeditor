#include "editor_canvas_tab.h"
#include "canvas.h"

#include <inode.h>

#include <QVBoxLayout>
#include <QGraphicsItem>

EditorCanvasTab::EditorCanvasTab(INode *node, QWidget *parent)
    : QWidget(parent),
      m_node(node)
{
    m_canvas = new Canvas;

    QVBoxLayout * layout = new QVBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(m_canvas);
    setLayout(layout);

    m_scene = new QGraphicsScene(this);
    m_scene->setSceneRect(-2000, -2000, 4000, 4000);
    m_canvas->setScene(m_scene);
}

EditorCanvasTab::~EditorCanvasTab()
{
    for(auto &item : m_scene->items())
        m_scene->removeItem(item);
}

QString EditorCanvasTab::name() const
{
    return m_node->name();
}

#include <QGraphicsItem>
void EditorCanvasTab::add_node(INode *node)
{
    m_scene->addItem(dynamic_cast<QGraphicsItem*>(node));
//    QRectF rect(-5, -5, 10, 10);
//    auto center = new QGraphicsRectItem(rect);
//    center->setPos(30, 20);
//    center->setBrush(Qt::red);
//    m_scene->addItem(center);
}
