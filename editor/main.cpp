#include <QApplication>

#include <config_constants.h>
#include <editor_main_window.h>

#include <composition.h>
#include <graphics_item_node.h>
#include <graphics_node_factory.h>
#include <graphics_rectangle.h>
#include <static_memory_cleaner.h>
#include <QDir>

int main(int argc, char *argv[])
{
    StaticMemoryCleaner cleaner;
    Q_UNUSED(cleaner);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QCoreApplication::setOrganizationName(ORGANIZATION_NAME);
    QCoreApplication::setOrganizationDomain(ORGANISATION_DOMAIN);
    QCoreApplication::setApplicationName(APP_NAME);
    QCoreApplication::setApplicationVersion(APP_VERSION);

    QApplication a(argc, argv);
    EditorMainWindow w;
    w.show();


#if 1
    Composition* comp = new Composition (new GraphicsNodeFactory, "Test Composition");
    if (true)
    {
        auto rect = new GraphicsRectangle();
        rect->set_name("Outer Rect");
        rect->set_position(QPointF(30, 20));
        rect->set_size(Size(200, 200));
        rect->properties();
    //    rect->set_opacity(0);
        rect->set_fill_color(QColor(186, 145, 123));

        auto internal_rect1 = new GraphicsRectangle();
        internal_rect1->set_name("Middle rect");
        internal_rect1->set_fill_color(Qt::darkBlue);
        internal_rect1->set_size(Size(150, 150));

        auto internal_rect2 = new GraphicsRectangle();
        internal_rect2->set_name("Inner Rect");
        internal_rect2->set_fill_color(Qt::darkMagenta);

        auto frame1 = rect->add_keyframe(PropertyType::Opacity, 2000, 0.10);
        auto frame3 = rect->add_keyframe(PropertyType::Opacity, 6000, 1);
        /*auto frame3 = */rect->add_keyframe(PropertyType::Opacity, 7000, 1);
        auto frame2 = rect->add_keyframe(PropertyType::Opacity, 4000, 0.35);
        //    auto frame0 = rect->add_keyframe(ProperyType::Opacity, 0, 0.0);
        auto frame4 = rect->add_keyframe(PropertyType::Opacity, 8000, 1);

        //    rect->remove_keyframe(ProperyType::Opacity, frame1);
        //    rect->remove_keyframe(ProperyType::Opacity, frame3);
        //    rect->remove_keyframe(ProperyType::Opacity, frame4);

        rect->add_keyframe(PropertyType::Scale, 0, Scale(1,1));
        KeyFrame * s1 = rect->add_keyframe(PropertyType::Scale, 2000, Scale(1.5,0.5));
        KeyFrame * s2 = rect->add_keyframe(PropertyType::Scale, 3000, Scale(1,1));
        rect->add_keyframe(PropertyType::Scale, 4000, Scale(0.5,1.5));
        rect->add_keyframe(PropertyType::Scale, 6000, Scale(1, 1));

        rect->update_keyframe(PropertyType::Scale, s2, 1500);

        rect->add_keyframe(PropertyType::Rotation, 0, 0);
        rect->add_keyframe(PropertyType::Rotation, 2000, 90);
        rect->add_keyframe(PropertyType::Rotation, 3000, 180);
        rect->add_keyframe(PropertyType::Rotation, 4000, -90);
        rect->add_keyframe(PropertyType::Rotation, 6000, -90);
        rect->add_keyframe(PropertyType::Rotation, 8000, 0);

        //    internal_rect1->add_keyframe(PropertyType::Rotation, 2000, 90);
        //    internal_rect1->add_keyframe(PropertyType::Rotation, 3000, 180);
        //    internal_rect1->add_keyframe(PropertyType::Rotation, 4000, -90);
        //    internal_rect1->add_keyframe(PropertyType::Rotation, 6000, -90);
        //    internal_rect1->add_keyframe(PropertyType::Rotation, 7000, 0);
        //    internal_rect1->add_keyframe(PropertyType::Rotation, 8000, 0);

        //    internal_rect2->add_keyframe(PropertyType::Rotation, 2000, 90);
        //    internal_rect2->add_keyframe(PropertyType::Rotation, 3000, 180);
        //    internal_rect2->add_keyframe(PropertyType::Rotation, 4000, -90);
        //    internal_rect2->add_keyframe(PropertyType::Rotation, 6000, -90);
        //    internal_rect2->add_keyframe(PropertyType::Rotation, 7000, 0);
        //    internal_rect2->add_keyframe(PropertyType::Rotation, 8000, 0);

        //    rect->animate_property(PropertyType::Opacity);
        rect->animate_property(PropertyType::Scale);
        rect->animate_property(PropertyType::Rotation);
        //    internal_rect1->animate_property(PropertyType::Rotation);
        //    internal_rect2->animate_property(PropertyType::Rotation);

        //    timeline->add_node(rect);
        //    timeline->add_node(internal_rect1);
        //    timeline->add_node(internal_rect2);

        rect->add_child(internal_rect1);
        rect->add_child(internal_rect2);

        comp->add_child(rect);
        w.open_compostion(comp);

        QDir homeDir = QDir::home();
        QFile file(homeDir.filePath("test.dat"));
        if( !file.open( QIODevice::WriteOnly ) )
            return -1;

        QDataStream stream( &file );
        stream.setVersion( QDataStream::Qt_5_12 );
        comp->save(stream);
        file.close();
    }
    else
    {
        QDir homeDir = QDir::home();
        QFile file(homeDir.filePath("test.dat"));
        if( !file.open( QIODevice::ReadOnly ) )
            return -1;


        QDataStream stream( &file );
        stream.setVersion( QDataStream::Qt_5_12 );

        comp->load(stream);
    }
#endif

//    for(auto item : comp.children())
//    {
//        timeline->add_node(item);
//        scene->addItem(static_cast<GraphicsItemNode*>(item));
//    }


//    timeline->update_size();
//    scroll->cornerWidget()->setFixedHeight(timeline->items_count()*Theme::row_height);
//    Property p1;
//    Property p2;
//    p1.add_sub_property(&p2);
//    p1.sub_properties();
//    p2.parent();
    return a.exec();
}
